package com.vtadrones.VTADrones.GeometryEditor;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;

import com.vtadrones.VTADrones.VTADronesState;
import com.vtadrones.VTADrones.BaseClasses.Feature;
import com.vtadrones.VTADrones.Map.Map;

import org.apache.cordova.CordovaWebView;

import java.lang.ref.WeakReference;

public class EditHandler {
	private WeakReference<Activity> weakActivity;
	private CordovaWebView cordovaWebView;
	public EditHandler(Activity activity){
		this.weakActivity = new WeakReference<Activity>(activity);
		
		if(activity != null){
			try{
				this.cordovaWebView = ((Map.CordovaMap) activity).getWebView();
			}catch(ClassCastException e){
				e.printStackTrace();
			}
		}
	}
	
	public void cancel(){
		FragmentActivity activity = null;
		
		try{
			activity = (FragmentActivity) weakActivity.get();
		}catch(ClassCastException e){
			e.printStackTrace();
		}
		
		if(activity != null){
			
			// Restore the geometry of the selectedFeature
			Feature selectedFeature = VTADronesState.getVTADronesState().isEditingFeature();
			
			// Exit modify mode
			Map.getMap().cancelEdit(cordovaWebView,
					selectedFeature.getOriginalGeometry());
			
			selectedFeature.restoreGeometry();
		}
	}
	
	public void done(){
		Map.getMap().getUpdatedGeometry(cordovaWebView);
	}
}
