package com.vtadrones.VTADrones.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vtadrones.VTADrones.Loaders.AddTilesetsListLoader;

public class AddTilesetsBroadcastReceiver extends BroadcastReceiver {
	private AddTilesetsListLoader loader;

	public AddTilesetsBroadcastReceiver(AddTilesetsListLoader loader){
		this.loader = loader;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		loader.onContentChanged();
	}

}
