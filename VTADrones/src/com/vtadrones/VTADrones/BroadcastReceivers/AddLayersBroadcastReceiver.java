package com.vtadrones.VTADrones.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vtadrones.VTADrones.Loaders.AddLayersListLoader;

public class AddLayersBroadcastReceiver extends BroadcastReceiver {
	private AddLayersListLoader loader;
	
	public AddLayersBroadcastReceiver(AddLayersListLoader loader){
		this.loader = loader;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		loader.onContentChanged();
	}

}
