package com.vtadrones.VTADrones.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vtadrones.VTADrones.Loaders.BaseLayerLoader;

public class BaseLayerBroadcastReceiver extends BroadcastReceiver {
	private BaseLayerLoader loader;
	
	public BaseLayerBroadcastReceiver(BaseLayerLoader baseLayerLoader){
		this.loader = baseLayerLoader;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		loader.onContentChanged();
	}

}
