package com.vtadrones.VTADrones.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vtadrones.VTADrones.Loaders.ServersListLoader;

public class ServerBroadcastReceiver extends BroadcastReceiver {
	private ServersListLoader loader;
	
	public ServerBroadcastReceiver(ServersListLoader serversListLoader){
		this.loader = serversListLoader;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		loader.onContentChanged();
	}

}
