package com.vtadrones.VTADrones.BroadcastReceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.AsyncTaskLoader;

import com.vtadrones.VTADrones.BaseClasses.Layer;

import java.util.ArrayList;

public class LayerBroadcastReceiver extends BroadcastReceiver {
	private AsyncTaskLoader<ArrayList<Layer>> loader;
	
	public LayerBroadcastReceiver(AsyncTaskLoader<ArrayList<Layer>> layersListLoader){
		this.loader = layersListLoader;
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		loader.onContentChanged();
	}

}
