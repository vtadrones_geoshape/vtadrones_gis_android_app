package com.vtadrones.VTADrones;

import com.vtadrones.VTADrones.BaseClasses.Feature;

public class VTADronesState {
	private VTADronesProject VTADronesProject;
	private String newAOI;
	private Feature feature;
	private String layerId;
	
	private VTADronesState(){
		this.VTADronesProject = VTADronesProject.getVTADronesProject();
		this.newAOI = null;
	}
	
	private static VTADronesState VTADronesState = null;
	
	public static VTADronesState getVTADronesState(){
		if(VTADronesState == null){
			VTADronesState = new VTADronesState();
		}
		
		return VTADronesState;
	}
	
	public boolean isCreatingProject(){
		return (VTADronesProject.getNewProject() != null);
	}
	
	public void setNewAOI(String newAOI){
		this.newAOI = newAOI;
	}
	
	public String getNewAOI(){
		return newAOI;
	}
	
	public boolean isSettingAOI(){
		return newAOI != null;
	}
	
	public void editingFeature(Feature feature, String layerId){
		this.feature = feature;
		this.layerId = layerId;
	}
	
	public void doneEditingFeature(){
		this.feature = null;
	}
	
	public Feature isEditingFeature(){
		return this.feature;
	}
	
	public String getLayerBeingEdited(){
		return this.layerId;
	}
}
