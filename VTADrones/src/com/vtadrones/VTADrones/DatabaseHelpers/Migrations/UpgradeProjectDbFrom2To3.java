package com.vtadrones.VTADrones.DatabaseHelpers.Migrations;

import android.database.sqlite.SQLiteDatabase;

import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.NotificationsTableHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.SyncTableHelper;

public class UpgradeProjectDbFrom2To3 implements Migration{
	
	public UpgradeProjectDbFrom2To3(){
		
	}
	
	public void migrate(SQLiteDatabase db){
		
		(new SyncTableHelper(db)).createTable();
		(new NotificationsTableHelper(db)).createTable();
	}
}
