package com.vtadrones.VTADrones.DatabaseHelpers.Migrations;

public class DatabaseVersionException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public DatabaseVersionException(String message){
		super(message);
	}
}
