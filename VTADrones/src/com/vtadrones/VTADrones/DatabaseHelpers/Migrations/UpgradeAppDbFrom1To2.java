package com.vtadrones.VTADrones.DatabaseHelpers.Migrations;

import android.database.sqlite.SQLiteDatabase;

import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.PreferencesHelper;

public class UpgradeAppDbFrom1To2 implements Migration{
	
	public void migrate(SQLiteDatabase db){
		
		PreferencesHelper.getHelper().createTable(db);
	}
}
