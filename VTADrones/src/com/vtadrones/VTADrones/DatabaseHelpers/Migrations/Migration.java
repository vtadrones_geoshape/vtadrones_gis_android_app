package com.vtadrones.VTADrones.DatabaseHelpers.Migrations;

import android.database.sqlite.SQLiteDatabase;

public interface Migration {

	public void migrate(SQLiteDatabase db);
}
