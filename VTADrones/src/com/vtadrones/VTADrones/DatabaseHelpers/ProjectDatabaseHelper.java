package com.vtadrones.VTADrones.DatabaseHelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.vtadrones.VTADrones.DatabaseHelpers.Migrations.Migration;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.FailedSync;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.LayersHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.NotificationsTableHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.PreferencesHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.SyncTableHelper;

import java.io.File;

public class ProjectDatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "VTADrones_project.db";
	private static int DATABASE_VERSION = 5;
	
	private String currentPath;
	
	private ProjectDatabaseHelper(Context context, String path){
		super(context, path + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
		this.currentPath = path;
	}
	
	private static ProjectDatabaseHelper helper = null;
	
	public static ProjectDatabaseHelper getHelper(Context context, String path, boolean reset){
		if(helper != null && 
				// path to the db isn't the same or resetting 
				(!path.equals(helper.getCurrentPath()) || reset)){
			
			resetConnection(context, path);
		}else if(helper == null){
			helper = new ProjectDatabaseHelper(context, path);
		}
		
		return helper;
	}
	
	@Override
	public void close() {
		super.close();
		
		helper = null;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		LayersHelper.getLayersHelper().createTable(db);
		PreferencesHelper.getHelper().createTable(db);
		FailedSync.getHelper().createTable(db);
		
		(new SyncTableHelper(db)).createTable();
		(new NotificationsTableHelper(db)).createTable();
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		int version = oldVersion;
		int updatedVersion = newVersion;
		
		while(version != updatedVersion){
			
			try {
				Class<?> clazz = Class.forName("com.vtadrones.VTADrones.DatabaseHelpers.Migrations.UpgradeProjectDbFrom"
						+ Integer.toString(version) + "To" 
						+ Integer.toString(++version));
				
				Migration migration = (Migration) clazz.newInstance();
				
				migration.migrate(db);
				
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}catch(ClassCastException e){
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onOpen(SQLiteDatabase db){
		super.onOpen(db);
		if(!db.isReadOnly()){
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}
	
	private static void resetConnection(Context context, String path){
		if(helper != null){
			helper.close();
			helper = new ProjectDatabaseHelper(context, path);
		}
	}
	
	private String getCurrentPath(){
		return this.currentPath;
	}
}
