package com.vtadrones.VTADrones.DatabaseHelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.vtadrones.VTADrones.DatabaseHelpers.Migrations.Migration;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.PreferencesHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.ServersHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.TilesetsHelper;
import com.vtadrones.VTADrones.ProjectStructure.ProjectStructure;

import java.io.File;

public class ApplicationDatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "VTADrones_application.db";
	private static int DATABASE_VERSION = 4;
	
	private ApplicationDatabaseHelper(Context context){
		super(context, ProjectStructure.getApplicationRoot() + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	private static ApplicationDatabaseHelper helper = null;
	
	public static ApplicationDatabaseHelper getHelper(Context context){
		if(helper == null){
			helper = new ApplicationDatabaseHelper(context);
		}
		
		return helper;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		ServersHelper.getServersHelper().createTable(db);
		PreferencesHelper.getHelper().createTable(db);
		TilesetsHelper.getTilesetsHelper().createTable(db);
	}

	@Override
	public void close() {
		super.close();
		
		helper = null;
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		int version = oldVersion;
		int updatedVersion = newVersion;
		
		while(version != updatedVersion){
			
			try {
				Class<?> clazz = Class.forName("com.vtadrones.VTADrones.DatabaseHelpers.Migrations.UpgradeAppDbFrom"
						+ Integer.toString(version) + "To" 
						+ Integer.toString(++version));
				
				Migration migration = (Migration) clazz.newInstance();
				
				migration.migrate(db);
				
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}catch(ClassCastException e){
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onOpen(SQLiteDatabase db){
		super.onOpen(db);
		if(!db.isReadOnly()){
			// Enable foreign key constraints
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}
}
