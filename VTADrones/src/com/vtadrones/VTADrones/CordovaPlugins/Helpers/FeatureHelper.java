package com.vtadrones.VTADrones.CordovaPlugins.Helpers;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.vtadrones.VTADrones.BaseClasses.Feature;
import com.vtadrones.VTADrones.Dialog.Dialogs.FeatureDialog.FeatureDialog;
import com.vtadrones.VTADrones.R;

public class FeatureHelper {
	private FragmentActivity activity;
	
	public FeatureHelper(FragmentActivity activity){
		this.activity = activity;
	}
	
	public void displayFeatureDialog(Feature feature, String layerId, boolean geomEdited, boolean isReadOnly){
		
		displayDialog(feature, layerId, geomEdited || feature.isNew(), isReadOnly);
	}
	
	private void displayDialog(Feature feature, String layerId, boolean startInEditMode, boolean isReadOnly){
		
		String title = feature.getFeatureType();
		
		FeatureDialog dialog = FeatureDialog.newInstance(title, 
				R.layout.feature_dialog, feature,
				layerId, startInEditMode, isReadOnly);
		
		FragmentManager manager = activity.getSupportFragmentManager();
		
		if(dialog != null) {
			dialog.show(manager, FeatureDialog.TAG);
		}
	}
}
