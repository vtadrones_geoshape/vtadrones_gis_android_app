package com.vtadrones.VTADrones.Dialog.Dialogs;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.vtadrones.VTADrones.VTADronesProject;
import com.vtadrones.VTADrones.ConnectivityListeners.ConnectivityListener;
import com.vtadrones.VTADrones.Dialog.VTADronesDialogFragment;
import com.vtadrones.VTADrones.Dialog.VTADronesDialogs;
import com.vtadrones.VTADrones.R;
import com.vtadrones.VTADrones.Util;

public class ProjectNameDialog extends VTADronesDialogFragment{
	private View view;
	private VTADronesDialogs VTADronesDialogs;
	private ConnectivityListener connectivityListener;
	
	public static ProjectNameDialog newInstance(String title, String ok, 
			String cancel, int layout, ConnectivityListener connectivityListener){
		
		final ProjectNameDialog frag = new ProjectNameDialog();
		
		frag.setTitle(title);
		frag.setOk(ok);
		frag.setCancel(cancel);
		frag.setLayout(layout);
		
		frag.connectivityListener = connectivityListener;
		
		frag.setValidatingClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				if(frag.connectivityListener != null && frag.connectivityListener.isConnected()){
					frag.onPositiveClick();
				}else{
					Util.showNoNetworkDialog(frag.getActivity());
				}
			}
		});
		
		return frag;
	}

	private VTADronesDialogs getVTADronesDialogs(){
		if(VTADronesDialogs == null){
			VTADronesDialogs = new VTADronesDialogs(getActivity().getApplicationContext(), getActivity().getApplicationContext().getResources(),
								getActivity().getSupportFragmentManager());
		}
		
		return VTADronesDialogs;
	}
	
	@Override
	public void onPositiveClick() {
		EditText projectNameField = (EditText) view.findViewById(R.id.project_name_edittext);
		VTADronesProject vtaDronesProject = VTADronesProject.getVTADronesProject();

		vtaDronesProject.createNewProject(projectNameField.getText().toString());
		
		getVTADronesDialogs().showAddLayersDialog(true, connectivityListener);
		
		dismiss();
	}

	@Override
	public void onNegativeClick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeCreateDialog(View view) {
		this.view = view;
	}
}
