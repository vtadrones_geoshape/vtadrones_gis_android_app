package com.vtadrones.VTADrones.Dialog.Dialogs;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.vtadrones.VTADrones.VTADronesProject;
import com.vtadrones.VTADrones.DatabaseHelpers.ApplicationDatabaseHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.PreferencesHelper;
import com.vtadrones.VTADrones.Dialog.VTADronesDialogFragment;
import com.vtadrones.VTADrones.Loaders.NotificationsLoader;
import com.vtadrones.VTADrones.Loaders.ProjectsListLoader;

public class SwitchProjectDialog extends VTADronesDialogFragment{
	private String newProjectName;
	
	public static SwitchProjectDialog newInstance(String title, String ok, 
			String cancel, int layout, String newProjectName){
		SwitchProjectDialog frag = new SwitchProjectDialog();
		
		frag.setTitle(title);
		frag.setOk(ok);
		frag.setCancel(cancel);
		frag.setLayout(layout);
		
		frag.newProjectName = newProjectName;
		
		return frag;
	}
	
	@Override
	public void onPositiveClick() {
		VTADronesProject.getVTADronesProject().setOpenProject(
				getActivity().getApplicationContext(),
				newProjectName);
		
		LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
			.sendBroadcast(new Intent(ProjectsListLoader.PROJECT_LIST_UPDATED));
		
		LocalBroadcastManager.getInstance(getActivity().getApplicationContext())
			.sendBroadcast(new Intent(NotificationsLoader.NOTIFICATIONS_UPDATED));
		
		SQLiteDatabase db = ApplicationDatabaseHelper.getHelper(getActivity().getApplicationContext()).getWritableDatabase();
		
		PreferencesHelper.getHelper().put(db, getActivity().getApplicationContext(), PreferencesHelper.SWITCHED_PROJECT, "true");
		
		this.getActivity().finish();
	}

	@Override
	public void onNegativeClick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeCreateDialog(View view) {
		
	}
}
