package com.vtadrones.VTADrones.Dialog.Dialogs.ChooseBaseLayer;

import com.vtadrones.VTADrones.BaseClasses.BaseLayer;

public interface BaseLayerUpdater {

	public void updateBaselayer(BaseLayer baseLayer);
}
