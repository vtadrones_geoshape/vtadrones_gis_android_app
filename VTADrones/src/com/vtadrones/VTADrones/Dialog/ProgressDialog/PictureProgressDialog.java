package com.vtadrones.VTADrones.Dialog.ProgressDialog;

import android.app.Activity;
import android.app.ProgressDialog;

import com.vtadrones.VTADrones.R;


public class PictureProgressDialog {
	
	private static ProgressDialog pictureProgressDialog = null;
	
	public static void show(final Activity activity) {
		
		activity.runOnUiThread(new Runnable(){
			
			@Override
			public void run(){
				
				String title = activity.getResources().getString(R.string.loading);
				String message = activity.getResources().getString(R.string.please_wait);
				
				pictureProgressDialog = ProgressDialog.show(activity, title, message);
			}
		});
	}
	
	public static void dismiss(final Activity activity) {
		activity.runOnUiThread(new Runnable(){
			@Override
			public void run(){
				if(pictureProgressDialog != null) {
					pictureProgressDialog.dismiss();
					pictureProgressDialog = null;
				}
			}
		});
	}
}
