package com.vtadrones.VTADrones.ConnectivityListeners;

public interface HasConnectivityListener {

	public ConnectivityListener getListener();
}
