package com.vtadrones.VTADrones.Activities;

import java.util.concurrent.ExecutorService;

public interface HasThreadPool {

	public ExecutorService getThreadPool();
}
