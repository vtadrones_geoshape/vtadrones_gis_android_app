package com.vtadrones.VTADrones;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.vtadrones.VTADrones.DatabaseHelpers.ProjectDatabaseHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.PreferencesHelper;
import com.vtadrones.VTADrones.ProjectStructure.ProjectStructure;

public class OOMWorkaround {
	private Activity activity;
	private Context context;
	
	public OOMWorkaround(Activity activity){
		this.activity = activity;
		this.context = activity.getApplicationContext();
	}
	
	// Don't keep a db because it will change when
	// the project gets switched.
	private SQLiteDatabase getProjectDatabase(){
		
		String projectName = VTADronesProject.getVTADronesProject()
				.getOpenProject(activity);
		
		String path = ProjectStructure.getProjectPath(projectName);
		
		return ProjectDatabaseHelper.getHelper(context, 
				path, false).getWritableDatabase();
	}
	
	public void resetSavedBounds(boolean isCreatingProject){
		
		SQLiteDatabase db = getProjectDatabase();
			
		PreferencesHelper.getHelper().put(db, context, 
			PreferencesHelper.SHOULD_ZOOM_TO_AOI, 
			Boolean.toString(!isCreatingProject));
		
		PreferencesHelper.getHelper().delete(db, context, PreferencesHelper.SAVED_BOUNDS);
		PreferencesHelper.getHelper().delete(db, context, PreferencesHelper.SAVED_ZOOM_LEVEL);
	}
	
	public void setSavedBounds(String bounds, String zoom, boolean isCreatingProject){
		SQLiteDatabase db = getProjectDatabase();
		
		PreferencesHelper.getHelper().put(db, context, PreferencesHelper.SAVED_BOUNDS, bounds);
		PreferencesHelper.getHelper().put(db, context, PreferencesHelper.SAVED_ZOOM_LEVEL, zoom);
	}
}
