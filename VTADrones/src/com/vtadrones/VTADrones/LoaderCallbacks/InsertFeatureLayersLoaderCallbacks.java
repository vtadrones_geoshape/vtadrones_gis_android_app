package com.vtadrones.VTADrones.LoaderCallbacks;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import com.vtadrones.VTADrones.BaseClasses.Layer;
import com.vtadrones.VTADrones.ListAdapters.VTADronesAdapter;
import com.vtadrones.VTADrones.Loaders.InsertFeaturesLayersLoader;

import java.util.ArrayList;

public class InsertFeatureLayersLoaderCallbacks implements LoaderManager.LoaderCallbacks<ArrayList<Layer>>{

	private VTADronesAdapter<ArrayList<Layer>> layerAdapter;
	private FragmentActivity activity;
	
	public InsertFeatureLayersLoaderCallbacks(FragmentActivity activity, 
			VTADronesAdapter<ArrayList<Layer>> adapter, int loaderId){
		this.layerAdapter = adapter;
		this.activity = activity;
		
		activity.getSupportLoaderManager().initLoader(loaderId, null, this);
	}
	
	@Override
	public Loader<ArrayList<Layer>> onCreateLoader(int id, Bundle bundle) {
        return new InsertFeaturesLayersLoader(getActivity());
	}

	protected FragmentActivity getActivity(){
		return activity;
	}
	
	@Override
	public void onLoadFinished(Loader<ArrayList<Layer>> loader, ArrayList<Layer> data) {
		Log.w("LayerLoaderCallbacks", "LayerLoaderCallbacks data length = " + data.size());
		layerAdapter.setData(data);
	}

	@Override
	public void onLoaderReset(Loader<ArrayList<Layer>> loader) {
		layerAdapter.setData(null);
	}	
}
