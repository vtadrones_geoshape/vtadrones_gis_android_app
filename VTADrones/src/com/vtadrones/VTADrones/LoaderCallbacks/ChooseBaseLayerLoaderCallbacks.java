package com.vtadrones.VTADrones.LoaderCallbacks;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.Loader;

import com.vtadrones.VTADrones.BaseClasses.Layer;
import com.vtadrones.VTADrones.ListAdapters.VTADronesAdapter;
import com.vtadrones.VTADrones.Loaders.ChooseBaseLayerLoader;

import java.util.ArrayList;

public class ChooseBaseLayerLoaderCallbacks extends LayerLoaderCallbacks {
	
	public ChooseBaseLayerLoaderCallbacks(FragmentActivity activity,
			VTADronesAdapter<ArrayList<Layer>> adapter, int loaderId) {
		super(activity, adapter, loaderId);
	}

	@Override
	public Loader<ArrayList<Layer>> onCreateLoader(int id, Bundle bundle) {
		// This is called when a new Loader needs to be created.  This
        // sample only has one Loader with no arguments, so it is simple.
        return new ChooseBaseLayerLoader(getActivity());
	}
}
