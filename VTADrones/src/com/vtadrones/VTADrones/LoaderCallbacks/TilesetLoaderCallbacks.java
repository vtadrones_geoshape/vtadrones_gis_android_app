package com.vtadrones.VTADrones.LoaderCallbacks;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;

import com.vtadrones.VTADrones.BaseClasses.Tileset;
import com.vtadrones.VTADrones.ListAdapters.VTADronesAdapter;
import com.vtadrones.VTADrones.Loaders.TilesetsListLoader;

import java.util.ArrayList;

public class TilesetLoaderCallbacks implements LoaderManager.LoaderCallbacks<ArrayList<Tileset>>{

	private VTADronesAdapter<ArrayList<Tileset>> tilesetAdapter;
	private FragmentActivity activity;

	public TilesetLoaderCallbacks(FragmentActivity activity,
								  VTADronesAdapter<ArrayList<Tileset>> adapter, int loaderId){
		this.tilesetAdapter = adapter;
		this.activity = activity;
		
		activity.getSupportLoaderManager().initLoader(loaderId, null, this);
	}
	
	@Override
	public Loader<ArrayList<Tileset>> onCreateLoader(int id, Bundle bundle) {
        return new TilesetsListLoader(getActivity());
	}

	protected FragmentActivity getActivity(){
		return activity;
	}
	
	@Override
	public void onLoadFinished(Loader<ArrayList<Tileset>> loader, ArrayList<Tileset> data) {
		Log.w("TilesetLoaderCallbacks", "TilesetLoaderCallbacks data length = " + data.size());
		tilesetAdapter.setData(data);
	}

	@Override
	public void onLoaderReset(Loader<ArrayList<Tileset>> loader) {
		tilesetAdapter.setData(null);
	}	
}
