package com.vtadrones.VTADrones.Loaders;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.util.SparseArray;

import com.vtadrones.VTADrones.BaseClasses.BaseLayer;
import com.vtadrones.VTADrones.BaseClasses.Layer;
import com.vtadrones.VTADrones.BaseClasses.Server;
import com.vtadrones.VTADrones.BaseClasses.Tileset;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.LayersHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.ServersHelper;
import com.vtadrones.VTADrones.DatabaseHelpers.TableHelpers.TilesetsHelper;

import java.util.ArrayList;

public class ChooseBaseLayerLoader extends LayersListLoader {
	
	public ChooseBaseLayerLoader(Activity activity) {
		super(activity);
	}

	@Override
	public ArrayList<Layer> loadInBackground() {
		updateProjectDbHelper();
		
		SQLiteDatabase db = getProjectDbHelper().getWritableDatabase();
		
		ArrayList<Layer> layers = LayersHelper.getLayersHelper().
				getAll(db);
		
		SparseArray<Server> servers = ServersHelper.getServersHelper().
				getAll(getAppDbHelper().getWritableDatabase());

		ArrayList<Tileset> tilesets = TilesetsHelper.getTilesetsHelper().getAll(getAppDbHelper().getWritableDatabase());
		
		layers = addServerInfoToLayers(layers, servers);

		for (int i = 0; i < tilesets.size(); i++) {
			if (tilesets.get(i).getFilesize() > 0)
				layers.add(new Layer(tilesets.get(i).toBaseLayer()));
		}

		layers.add(new Layer(BaseLayer.createOSMBaseLayer()));
		
		return layers;
	}
}
