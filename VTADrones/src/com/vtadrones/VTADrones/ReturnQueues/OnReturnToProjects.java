package com.vtadrones.VTADrones.ReturnQueues;


public class OnReturnToProjects extends VTADronesQueue{
	
	private OnReturnToProjects(){
		
		super();
	}
	
	private static OnReturnToProjects onReturnToProjects;
	
	public static OnReturnToProjects getInstance(){
		
		if(onReturnToProjects == null){
			onReturnToProjects = new OnReturnToProjects();
		}
		
		return onReturnToProjects;
	}
}
