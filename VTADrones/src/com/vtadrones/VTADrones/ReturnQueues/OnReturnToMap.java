package com.vtadrones.VTADrones.ReturnQueues;


public class OnReturnToMap extends VTADronesQueue{
	
	private OnReturnToMap(){
		
		super();
	}
	
	private static OnReturnToMap onReturnToMap;
	
	public static OnReturnToMap getInstance(){
		
		if(onReturnToMap == null){
			onReturnToMap = new OnReturnToMap();
		}
		
		return onReturnToMap;
	}
}
