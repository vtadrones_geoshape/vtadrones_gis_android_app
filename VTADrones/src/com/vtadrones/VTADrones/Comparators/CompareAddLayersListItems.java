package com.vtadrones.VTADrones.Comparators;

import com.vtadrones.VTADrones.BaseClasses.Layer;

import java.util.Comparator;

public class CompareAddLayersListItems implements Comparator<Layer>{

	@Override
	public int compare(Layer item1, Layer item2) {
		
		return item1.getLayerTitle().compareToIgnoreCase(item2.getLayerTitle());
	}
}
