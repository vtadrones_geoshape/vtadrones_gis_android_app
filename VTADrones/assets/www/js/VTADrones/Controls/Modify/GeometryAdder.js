VTADrones.GeometryAdder = function(map, modifyLayer, geometryType, featureAddedHandler){
	this.map = map;
	this.modifyLayer = modifyLayer;
	this.geometryType = geometryType;
	this.featureAddedHandler = featureAddedHandler;
	
	this.sketchStarted = false;
	this.sketchCompleted = false;
	
	this.vertexCount = 0;
	
	this.insertController = null;
	
	this.registerEvents();
	
	this.addInsertController();
};

VTADrones.GeometryAdder.prototype.registerEvents = function(){
	this.modifyLayer.events.register("sketchstarted", this, this.onSketchStarted);
	this.modifyLayer.events.register("sketchcomplete", this, this.onSketchComplete);
	this.modifyLayer.events.register("featureadded", this, this.onFeatureAdded);
};

VTADrones.GeometryAdder.prototype.onFeatureAdded = function(event){
	
	if(VTADrones.Util.existsAndNotNull(this.insertController) && this.insertController.active){
		this.removeInsertController();
		
		this.featureAddedHandler.call(this, event.feature);
	}
};

VTADrones.GeometryAdder.prototype.onSketchStarted = function(){
	console.log("onSketchStarted");
	
	this.sketchStarted = true;
};

VTADrones.GeometryAdder.prototype.onSketchComplete = function(){
	
	console.log("onSketchComplete");
	this.sketchCompleted = true;
};

VTADrones.GeometryAdder.prototype.addInsertController = function(){
		
	var type = VTADrones.Geometry.type;
	
	var options = {};
	
	var handler = null;
	
	switch(this.geometryType){
		case type.POINT:
			
			handler = OpenLayers.Handler.Point;
			
			break;
			
		case type.LINE:
			
			handler = OpenLayers.Handler.Path;
			
			break;
			
		case type.POLYGON:
			
			handler = OpenLayers.Handler.Polygon;
			
			break;
		
		case type.MULTIPOINT:
			
			handler = OpenLayers.Handler.Point;
			
			break;
			
		case type.MULTILINE:
			
			handler = OpenLayers.Handler.Path;
			
			break;
		case type.MULTIPOLYGON:
			
			handler = OpenLayers.Handler.Polygon;
			
			break;
			
		case type.GEOMETRY:
			
			handler = OpenLayers.Handler.Point;
			
			break;
			
		case type.MULTIGEOMETRY:
			
			handler = OpenLayers.Handler.Point;
			
			break;
			
		default:
			
	}
	
	this.insertController = new OpenLayers.Control.DrawFeature(this.modifyLayer, handler, options);
	
	this.map.addControl(this.insertController);
	
	this.insertController.activate();
};

VTADrones.GeometryAdder.prototype.removeInsertController = function(){
	
	if(VTADrones.Util.existsAndNotNull(this.insertController) && this.insertController.active){
		this.insertController.deactivate();
		
		this.map.removeControl(this.insertController);
	}
};

VTADrones.GeometryAdder.prototype.finish = function(){
	
	if(!this.sketchCompleted && VTADrones.Util.existsAndNotNull(this.insertController)
			&& (this.geometryType !== VTADrones.Geometry.type.POINT
			&& this.geometryType !== VTADrones.Geometry.type.MULTIPOINT)){
	
		this.insertController.finishSketch();
	}
};