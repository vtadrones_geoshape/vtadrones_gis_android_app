VTADrones.Controls.ControlPanel = (function(){
	var selectedFeature = null;
	
	var selectControl = null;
	
	var modifyControl = null;
	
	var insertControl = null;
	
	var controlPanelHelper = new VTADrones.ControlPanelHelper();
	
	var mode = VTADrones.ControlPanelHelper.prototype.CONTROLS.SELECT;
	
	var cancel = false;
	
	var oomCleared = true;
	
	var onFinishedInserting = null;
	
	var _endInsertMode = function(){
		
		if(VTADrones.Util.existsAndNotNull(insertControl)){
			insertControl.deactivate();
			
			insertControl = null;
		}
	};
	
	var _startInsertMode = function(layerId, _geometryType){
		
		var olLayer = VTADrones.Layers.getLayerById(
				layerId, VTADrones.Layers.type.WFS);
		
		var geometryType = VTADrones.Geometry.getGeometryType(layerId, _geometryType);
		
		var geometryTypeName = VTADrones.Geometry.getGeometryName(geometryType);
		
		var context = VTADrones.Controls.ControlPanel;
		
		var schema = VTADrones.getLayerSchemas()[layerId];
		
		if(schema === null || schema === undefined){
			throw "VTADrones.Controls.ControlPanel _startInsertMode - "
				"could not get schema for layer id '" + layerId + "'";
		}
		
		controlPanelHelper.set(0, layerId, controlPanelHelper.CONTROLS.INSERT, 0, geometryTypeName, null, function(){
			
			var map = VTADrones.Map.getMap();
			selectControl.deactivate();
			insertControl = new VTADrones.Controls.Insert(olLayer, map,
					geometryType, function(feature){
				
				if(VTADrones.Util.existsAndNotNull(feature)){
				
					if(geometryType === VTADrones.Geometry.type.MULTIPOINT
							|| geometryType === VTADrones.Geometry.type.MULTILINE
							|| geometryType === VTADrones.Geometry.type.MULTIPOLYGON){
						
						if(!VTADrones.Util.existsAndNotNull(feature.metadata)){
							feature.metadata = {};
						}
						
						feature.metadata[VTADrones.FeatureTableHelper.PART_OF_MULTI] = true;
					}
						
					_endInsertMode();
					
					mode = VTADrones.ControlPanelHelper.prototype.CONTROLS.INSERT;

					selectControl.activate();

					selectControl.select(feature);
					
					selectedFeature = feature;
					
					startModifyMode(feature, function(){
						if(VTADrones.Util.existsAndNotNull(onFinishedInserting)){
							
							onFinishedInserting();
							
							onFinishedInserting = null;
						}
					});
				}else{
					
					_endInsertMode();
					
					if(VTADrones.Util.existsAndNotNull(onFinishedInserting)){
						
						onFinishedInserting();
						
						onFinishedInserting = null;
					}
				}
			});
		}, function(e){
			console.log("start insert mode error", e.stack);
		});
	};
	
	var startModifyMode = function(feature, onStartedModifyMode){
		
		var featureId = null;
		
		if(VTADrones.Util.existsAndNotNull(feature.metadata)){
			featureId = feature.metadata[VTADrones.FeatureTableHelper.ID];
		}
		
		var layerId = VTADrones.Util.getLayerId(feature.layer);
		
		var geometryType = VTADrones.Geometry.getGeometryType(layerId);
		
		var geometryTypeName = VTADrones.Geometry.getGeometryName(geometryType);
		
		if(geometryType === VTADrones.Geometry.type.MULTIGEOMETRY && feature.geometry.CLASS_NAME !== "OpenLayers.Geometry.Collection"){
			
			var collection = new OpenLayers.Geometry.Collection([feature.geometry]);
			
			feature.geometry = collection;
		}
		
		var wktGeometry = VTADrones.Geometry.getNativeWKT(feature, layerId);
		
		var schema = VTADrones.getLayerSchemas()[layerId];
		
		modifyControl = new VTADrones.Controls.Modify(VTADrones.Map.getMap(),
				feature.layer, feature, schema);
		
		controlPanelHelper.set(featureId, layerId,
				controlPanelHelper.CONTROLS.MODIFY, wktGeometry, geometryTypeName, null, function(){
			
			selectControl.deactivate();
			
			modifyControl.activate();
			
			mode = VTADrones.ControlPanelHelper.prototype.CONTROLS.MODIFY;
			
			if(VTADrones.Util.funcExists(onStartedModifyMode)){
				onStartedModifyMode();
			}
		}, function(e){
			console.log("start modify mode error", e);
		});
	};
	
	var removeFeature = function(feature){
		var layer = feature.layer;
		
		layer.removeFeatures([feature]);
	};
	
	var onSelect = function(feature){
		console.log("feature selected and selectedFeature = ", selectedFeature, feature);
		
		console.log("controlPanel onSelect modifyControl is", modifyControl);
		
		selectedFeature = feature;
		
		if(VTADrones.Util.existsAndNotNull(feature.layer)
				// Account for the layers created by the draw feature control/handler
				&& (feature.layer.name.indexOf("OpenLayers") === -1)){
			
			var _mode = mode;
			var _cancel = cancel;
			
			// Make sure the mode related variables are reset
			mode = VTADrones.ControlPanelHelper.prototype.CONTROLS.SELECT;
			cancel = false;
			
			var featureId = null;
			
			if(VTADrones.Util.existsAndNotNull(feature.metadata)){
				featureId = feature.metadata[VTADrones.FeatureTableHelper.ID];
			}
			
			var layerId = VTADrones.Util.getLayerId(feature.layer);
			
			var exec = function(){
				if(VTADrones.Util.existsAndNotNull(selectedFeature.metadata) && selectedFeature.metadata["modified"]){
					delete selectedFeature.metadata["modified"];
				}else{
					VTADrones.Cordova.featureSelected(
							feature.layer.protocol.featureType,
							featureId,
							layerId,
							feature,
							_mode,
							_cancel
					);
				}
			};
			
			if(VTADrones.Util.existsAndNotNull(featureId)){
				
				controlPanelHelper.set(featureId, layerId, controlPanelHelper.CONTROLS.SELECT, 0, null, null, function(){
					
					oomCleared = false;
					
					exec();
				}, function(e){
					console.log("Error saving select mode", e);
				});
			}else{
				exec();
			}
		}
	};
	
	var onUnselect = function(feature){
		
		console.log("feature unselected: ", feature);
		// Unselect all features (added for multigeometries)
		selectControl.unselect();
		
		// If the modifyControl is null,
		// make sure selectedFeature is
		// cleared out.
		if(modifyControl === null){
			selectedFeature = null;
			
			if(!oomCleared){
				// Starting to clear out so flag
				// it for already cleared
				oomCleared = true;
				
				controlPanelHelper.clear(function(){
					console.log("control panel cleared successfully");
					VTADrones.Cordova.featureUnselected();
				}, function(e){
					console.log("Couldn't clear the control panel...", e);
					oomCleared = false;
				});
			}
		}
	};
	
	selectControl = new VTADrones.Controls.Select(onSelect, onUnselect);
	
	return {
		registerMapListeners: function(){
			selectControl.registerMapListeners();
		},
		
		enterModifyMode: function(feature, onStartedModifyMode){
			try{
				if(feature === null 
						|| feature === undefined){
					
					feature = selectedFeature;
				}
				
				if(feature === null || feature === undefined){
					throw "ControlPanel.js feature should not be " + feature;
				}
				
				startModifyMode(feature, onStartedModifyMode);
			}catch(e){
				console.log("enterModifyMode error", e);
			}
		},
		
		exitModifyMode: function(onExitModify){
			
			if(!VTADrones.Util.existsAndNotNull(modifyControl)){
				
				if(VTADrones.Util.existsAndNotNull(onExitModify)){
					onExitModify();
				}
				
				return;
			}
			
			modifyControl.done(function(){
				
				modifyControl = null;
				
				selectControl.activate();
				
				if(VTADrones.Util.existsAndNotNull(selectedFeature)){
					
					if(!VTADrones.Util.existsAndNotNull(selectedFeature.metadata)){
						selectedFeature.metadata = {};
					}
					
					selectedFeature.metadata["modified"] = true;
					
					selectControl.select(selectedFeature);
				}
				
				try{
					if(VTADrones.Util.existsAndNotNull(onExitModify)){
						onExitModify();
					}
				}catch(e){
					console.log(e.stack);
				}
			});
		},
		
		unselect: function(){
			console.log("ControlPanel.unselect()");
			selectControl.unselect();
			
			if(modifyControl === null){
				selectedFeature = null;
				
				controlPanelHelper.clear();
			}
		},
		
		/**
		 * Restore the geometry to its original location,
		 * and exit modify mode.
		 */
		cancelEdit: function(wktGeometry){
			console.log("ControlPanel.cancelEdit wktGeometry = " + wktGeometry);
			
			modifyControl.cancel(wktGeometry, function(){
				//modifyControl.deactivate();
				
				modifyControl = null;
				
				// Reactivate the selectControl
				selectControl.activate();
				
				//console.log("selectControl active: " + selectControl.isActive());
				
				if(!VTADrones.Util.existsAndNotNull(selectedFeature.metadata)){
					selectedFeature.metadata = {};
				}
				
				selectedFeature.metadata["modified"] = true;
				
				// Reselect the selectedFeature
				selectControl.select(selectedFeature);
			});
		},
		
		getSelectedFeature: function(){
			return selectedFeature;
		},
		
		startInsertMode: function(layerId, geometryType){
			
			_startInsertMode(layerId, geometryType);
		},
		
		finishGeometry: function(){
			if(VTADrones.Util.existsAndNotNull(insertControl)){
				insertControl.finishGeometry();
			}
		},
		
		finishInserting: function(_onFinishedInserting){
			
			if(VTADrones.Util.existsAndNotNull(insertControl)){
				
				onFinishedInserting = _onFinishedInserting;
				
				insertControl.finishInserting();
			}else{
				
				_onFinishedInserting();
				
				onFinishedInserting = null;
			}
		},
		
		getInsertControl: function(){
			return insertControl;
		},
		
		moveSelectedFeature: function(wktGeometry){
			
			if(wktGeometry === null || wktGeometry === undefined){
				throw "VTADrones.Controls.ControlPanel - couldn't restore original"
				+ " geometry because wktGeometry is " + wktGeometry;
			} 
			
			if(selectedFeature === null || selectedFeature === undefined){
				throw "VTADrones.Controls.Select - couldn't restore original"
				+ " geometry because selectedFeature is " + selectedFeature;
			}
			
			var geomFeature = VTADrones.Geometry.readWKT(wktGeometry);
			
			var olLayer = selectedFeature.layer;
			
			var layerId = VTADrones.Util.getLayerId(olLayer);
			
			var schema = VTADrones.getLayerSchemas()[layerId];
			
			var srid = VTADrones.Map.getMap().projection.projCode;
			
			geomFeature.geometry.transform(new OpenLayers.Projection(schema.getSRID()),
					new OpenLayers.Projection(srid));
			
			olLayer.removeFeatures([selectedFeature]);
			
			selectedFeature.geometry = geomFeature.geometry;
			
			olLayer.addFeatures([selectedFeature]);
		},
		/**
		 * @param { OpenLayers.Feature.Vector } feature The feature to select.
		 */
		select: function(feature){
			selectControl.select(feature);
		},
		
		setSelectedFeature: function(feature){
			selectedFeature = feature;
		},
		
		setMode: function(_mode){
			mode = _mode;
		},
		
		getMode: function(){
			return mode;
		},
		
		getCancel: function(){
			return cancel;
		},
		
		isAddingPart: function(){
			
			if(VTADrones.Util.existsAndNotNull(modifyControl)){
				
				VTADrones.Cordova.isAddingGeometryPart(modifyControl.isAddingPart());
			}
		},
		
		addPart: function(){
			console.log("ControlPanel.addPart");
			
			modifyControl.beginAddPart();
		},
		
		removePart: function(){
			console.log("ControlPanel.removePart");
			
			modifyControl.removePart();
		},
		
		addGeometry: function(geometryType){
			console.log("ControlPanel.addGeometry: " + geometryType);
			
			modifyControl.beginAddGeometry(geometryType);
		},
		
		removeGeometry: function(){
			console.log("ControlPanel.removeGeometry");
			
			modifyControl.removeGeometry();
		},
		
		getModifyControl: function(){
			return modifyControl;
		},
		
		selectGeometryPartByIndexChain: function(indexChain){
			
			modifyControl.selectGeometryPartByIndexChain(indexChain);
		}
	};
})();