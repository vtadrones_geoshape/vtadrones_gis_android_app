var app = (function() {
	
	var waitFuncs = [];
	var VTADronesInitialized = false;
	
	/**
	 * On device ready
	 */
	var onDeviceReady = function() {
        var appDb = VTADrones.ApplicationDbHelper.getDatabase();
	    VTADrones.PreferencesHelper.get(appDb, VTADrones.NO_CON_CHECKS, this, function(value) {
            if (value === 'true') {
                VTADrones.isOnline(true);
            } else {
                VTADrones.isOnline('onLine' in navigator && navigator.onLine);
            }
        });
	    
	    var context = this;
	    
		VTADrones.Init(function() {
			
			// Get the file system for use in TileUtil.js
			VTADrones.FileSystem.setFileSystem(function(fileSystem){
				
				var baseLayerLoader = new VTADrones.Loaders.BaseLayer();
				
				// Make sure the directories for storing tiles exists
				baseLayerLoader.load(function(baseLayer){

					// Get the saved bounds and zoom
					VTADrones.Cordova.Project.getSavedBounds(function(savedBounds, savedZoom){

						// Get the locale
						navigator.globalization.getLocaleName(function(locale){
							var localeCode = 'en';
							if(locale.value.indexOf('es') >= 0){
								localeCode = 'es'
							}
							VTADrones.Localization.setLocale(localeCode);

							var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();

							// Get the AOI to check to see if it's been set
							VTADrones.PreferencesHelper.get(projectDb, VTADrones.AOI, context, function(_aoi){

								var bounds = null;

								VTADrones.PreferencesHelper.get(appDb, VTADrones.SWITCHED_PROJECT, context, function(value) {
									if(value !== "true" && savedBounds !== null && savedBounds !== undefined
											&& savedZoom !== null
											&& savedZoom !== undefined){

										bounds = savedBounds.split(',');

										if(_aoi !== null && _aoi !== undefined
												&& _aoi !== ""){

											VTADrones.aoiHasBeenSet(true);
										}

										VTADrones.Map.zoomToExtent(bounds[0],
												bounds[1], bounds[2],
												bounds[3], savedZoom);
									}else{

										if(_aoi !== null && _aoi !== undefined
												&& _aoi !== ""){

											VTADrones.aoiHasBeenSet(true);

											bounds = _aoi.split(',');

											VTADrones.Map.zoomToExtent(bounds[0],
													bounds[1], bounds[2],
													bounds[3]);
										}
									}
									VTADrones.PreferencesHelper.put(appDb, VTADrones.SWITCHED_PROJECT, "false", context);
								}, function(e){
									console.log("Could not read SWITCHED_PROJECT variable from Preferences database: " + JSON.stringify(e));
								});

								VTADrones.Cordova.OOM_Workaround
									.registerMapListeners();

								VTADrones.Controls.ControlPanel
									.registerMapListeners();

								VTADrones.setTileUtil(
									new VTADrones.TileUtil(
										VTADrones.ProjectDbHelper.getProjectDatabase(),
										VTADrones.Map.getMap()
									)
								);

								VTADrones.Loaders.LayersLoader.addEventTypes();

								VTADrones.Loaders.LayersLoader.load(function(){

									VTADrones.PreferencesHelper.get(projectDb, VTADrones.ALWAYS_SHOW_LOCATION, context, function(alwaysShowLocation){

										if(alwaysShowLocation === true || alwaysShowLocation === "true"){

											var map = VTADrones.Map.getMap();

											var aoiLayer = map.getLayersByName(VTADrones.AOI)[0];

											if(VTADrones.Util.existsAndNotNull(aoiLayer)){

												VTADrones.findme = new VTADrones.FindMe(map, aoiLayer);

												VTADrones.findme.watchLocation(function(e){

													VTADrones.Cordova.alertGeolocationError();
												});
											}else{
												console.log("There is no aoi layer!");
											}
										}
									}, function(e){ console.log((VTADrones.Util.existsAndNotNull(e.stack)) ? e.stack : e); });

									//findMeOOM();

									VTADrones.Cordova.appFinishedLoading();
								}, function(e){
									console.log("Could not load layers during initialization: " + JSON.stringify(e));
								});

								for ( var i = 0; i < waitFuncs.length; i++) {
									waitFuncs[i].call();
								}

								VTADronesInitialized = true;
							});
						}, function(e){
							console.log("Error initializing VTADrones while getting the locale", e);
						});
					}, function(e){
						console.log("Error initializing VTADrones while getting saved bounds", e);
					});
				}, function(e){
					console.log("Error initializing VTADrones loading base layer", e);
				});
			}, function(e){
				console.log("Error initializing VTADrones - ", e);
			});
		});
	};
	
	var onOnline = function(){
		app.waitForVTADronesInit(function(){
			VTADrones.isOnline(true);
		});
	};
	
	var onOffline = function(){
		app.waitForVTADronesInit(function(){
			var appDb = VTADrones.ApplicationDbHelper.getDatabase();
            VTADrones.PreferencesHelper.get(appDb, VTADrones.NO_CON_CHECKS, this, function(value) {
                if (value === 'true') {
                    VTADrones.isOnline(true);
                } else {
                    VTADrones.Layers.toggleWMSLayers(false);
                    VTADrones.isOnline(false);
                }
            });
		});
	};
	
	/**
	 * Bind event listeners
	 */
	var bindEvents = function() {
		document.addEventListener('deviceready', onDeviceReady, false);
		document.addEventListener('online', onOnline, false);
		document.addEventListener('offline', onOffline, false);
	};
	
	/**
	 * Initialize the app
	 */
	var Init = function() {
		bindEvents();
	};
	
	Init();
	
	return {
		waitForVTADronesInit : function(func) {
			if (!VTADronesInitialized) {
				waitFuncs.push(func);
			} else {
				func.call();
			}
		},
		
		zoomToFeature: function(layerId, fid){
			
			var map = VTADrones.Map.getMap();
			
			var layer = VTADrones.Layers.getLayerById(layerId, VTADrones.Layers.type.WFS);
			
			var feature = layer.getFeatureByFid(fid);
			
			if(VTADrones.Util.existsAndNotNull(feature)){
				feature.geometry.calculateBounds();
				var bounds = feature.geometry.getBounds();
				
				var zoomForExtent = map.getZoomForExtent(bounds);
				
				if(zoomForExtent > 22){
					
					var centroid = feature.geometry.getCentroid();
					
					map.setCenter(new OpenLayers.LonLat(centroid.x, centroid.y), 22);
				}else{
					
					map.zoomToExtent(bounds);
				}
				
				feature.renderIntent = "select";
				
				layer.drawFeature(feature);
			}
		},
		
		showWMSLayersForServer: function(serverId){
			
			var schemas = VTADrones.getLayerSchemas();
			var schema = null;
			var layer = null;
			
			for(var key in schemas){
				
				schema = schemas[key];
				
				if(schema.getServerId() == serverId){
					
					layer = VTADrones.Layers.getLayerById(schema.getLayerId(), VTADrones.Layers.type.WMS);
					
					if(VTADrones.Util.existsAndNotNull(layer)){
						
						layer.setVisibility(true);
					}
				}
			}
		}
	};
})();