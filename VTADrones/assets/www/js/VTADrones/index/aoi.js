var app = (function() {

	var waitFuncs = [];
	var VTADronesInitialized = false;
	
	/**
	 * On device ready
	 */
	var onDeviceReady = function() {
		VTADrones.Init(function() {
			
			// Get the file system for use in TileUtil.js
			VTADrones.FileSystem.setFileSystem(function(){
				
				var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
				
				// Get the AOI to check to see if it's been set
				VTADrones.PreferencesHelper.get(projectDb, VTADrones.SHOULD_ZOOM_TO_AOI, this, function(shouldZoomToAOI){
					
					VTADrones.Cordova.Project.getSavedBounds(function(savedBounds, savedZoom){
						
						var bounds = null;
						
						if(savedBounds !== null && savedBounds !== undefined 
								&& savedZoom !== null 
								&& savedZoom !== undefined){
							
							bounds = savedBounds.split(',');
							
							VTADrones.Map.zoomToExtent(bounds[0],
									bounds[1], bounds[2], 
									bounds[3], savedZoom);
						}else if(shouldZoomToAOI){
							VTADrones.Cordova.Project.zoomToAOI(null, function(e){
								console.log("Error initialing VTADrones while getting aoi");
							});
						}else{
							VTADrones.Cordova.Project.zoomToDefault();
						}
						
						VTADrones.Cordova.OOM_Workaround
						.registerMapListeners();
					
						VTADrones.setTileUtil(
							new VTADrones.TileUtil(
								VTADrones.ProjectDbHelper.getProjectDatabase(),
								VTADrones.Map.getMap()
							)
						);
						
						VTADrones.Layers.removeAllLayers();
			            //aoi layer needs to be added here so geolocation will work
			            var map = VTADrones.Map.getMap();
			            var layer = new OpenLayers.Layer.Vector(VTADrones.AOI);
			            
			            map.addLayer(layer);
					
						VTADrones.Layers.addDefaultLayer(true);
					
						for ( var i = 0; i < waitFuncs.length; i++) {
							waitFuncs[i].call();
						}

						VTADronesInitialized = true;
					
						VTADrones.Cordova.appFinishedLoading();
					}, function(e){
						console.log("Error initializing VTADrones while getting saved bounds", e);
					});
				}, function(e){
					console.log("Error initializing VTADrones while getting "
							+ VTADrones.SHOULD_ZOOM_TO_AOI, e);
				});
			}, function(e){
				console.log("Error getting file system", e);
			});
		});
	};
	
	/**
	 * Bind event listeners
	 */
	var bindEvents = function() {
		document.addEventListener('deviceready', onDeviceReady, false);
	};
	
	/**
	 * Initialize the app
	 */
	var Init = function() {
		bindEvents();
	};
	
	Init();
	
	return {
		waitForVTADronesInit : function(func) {
			if (!VTADronesInitialized) {
				waitFuncs.push(func);
			} else {
				func.call();
			}
		}
	};
})();