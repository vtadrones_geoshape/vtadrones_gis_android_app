VTADrones.StoreVectorToSync = function(_map, _downloadOnly, _specificSchemas, _onSuccess){
	
	this.onSuccess = _onSuccess;
	
	this.map = _map;
	
	this.downloadOnly = _downloadOnly;
	
	this.layers = this.getLayers();

	this.specificSchemas = _specificSchemas;
	
	this.index = -1;
	
	this.failedToStore = null;
};

VTADrones.StoreVectorToSync.prototype.getLayers = function(){
	
	if(this.downloadOnly === true || this.downloadOnly === "true"){
		
		var layers = [];
		
		if(VTADrones.Util.existsAndNotNull(this.specificSchemas)){
			for(var i = 0; i < this.specificSchemas.length; i++){
				
				this.addVectorLayerById(layers, this.specificSchemas[i].getLayerId());
			}
		}

		return layers;
	}else{
		return this.map.getLayersByClass("OpenLayers.Layer.Vector");
	}
};

VTADrones.StoreVectorToSync.prototype.addVectorLayerById = function(layers, layerId){
	var layer = VTADrones.Layers.getLayerById(layerId, VTADrones.Layers.type.WFS);
	
	if(VTADrones.Util.existsAndNotNull(layer)){
		layers.push(layer);
	}
};

VTADrones.StoreVectorToSync.prototype.pop = function(){
	
	if(++this.index < this.layers.length){
		
		var layer = this.layers[this.index];
		
		if(layer.name !== VTADrones.AOI){
			
			return layer;
		}
		
		return this.pop();
	}
	
	return undefined;
};

VTADrones.StoreVectorToSync.prototype.onComplete = function(){
	
	if(VTADrones.Util.funcExists(this.onSuccess)){
		this.onSuccess(this.failedToStore);
	}
};

VTADrones.StoreVectorToSync.prototype.addToFailed = function(failedId){
	
	if(VTADrones.Util.existsAndNotNull(failedId)){
		
		if(!VTADrones.Util.existsAndNotNull(this.failedToStore)){
			
			this.failedToStore = [];
		}
		
		this.failedToStore.push(failedId);
	}
};

VTADrones.StoreVectorToSync.prototype.startStore = function(){
	this.storeNext();
};

VTADrones.StoreVectorToSync.prototype.storeNext = function(){
	
	var olLayer = this.pop();
	
	if(olLayer !== undefined){
		this.store(olLayer);
	}else{
		this.onComplete();
	}
};

VTADrones.StoreVectorToSync.prototype.store = function(olLayer){
	
	var context = this;
	
	var key = VTADrones.Util.getLayerId(olLayer);
	
	var dataType = VTADrones.FailedSyncHelper.DATA_TYPES.VECTOR;
	var syncType = VTADrones.FailedSyncHelper.SYNC_TYPES.DOWNLOAD;
	
	var onFailure = function(e){
		
		context.addToFailed(key);
		
		context.storeNext();
	};
	
	VTADrones.FailedSyncHelper.insert(key, dataType, syncType, key, function(){
		
		if(context.downloadOnly !== true && context.downloadOnly !== "true"){
			syncType = VTADrones.FailedSyncHelper.SYNC_TYPES.UPLOAD;
			
			VTADrones.FailedSyncHelper.insert(key, dataType, syncType, key, function(){
				
				context.storeNext();
				
			}, function(e){
				
				onFailure(e);
			});
		}else{
			context.storeNext();
		}
		
	}, function(e){
		
		onFailure(e);
	});
};


