VTADrones.StoreMediaFromFeature = function(_feature, _schema, _onSuccess){
	this.feature = _feature;
	this.schema = _schema;
	this.index = -1;
	this.onSuccess = _onSuccess;
	this.failedToStore = null;
	
	this.featureMedia = [];
	
	var media = this.getMediaFromOlFeature(this.feature);
	
	if(VTADrones.Util.existsAndNotNull(media)){
		this.featureMedia = media;
	}
};

VTADrones.StoreMediaFromFeature.prototype.getMediaFromOlFeature = function(olFeature){
	
	var attributeValue = olFeature.attributes[this.schema.getMediaColumn()];
	
	var parsed = null;
	
	if(VTADrones.Util.existsAndNotNull(attributeValue) && attributeValue !== ""){
		parsed = JSON.parse(attributeValue);
	}
	
	return parsed;
};

VTADrones.StoreMediaFromFeature.prototype.storeComplete = function(){
	
	if(VTADrones.Util.funcExists(this.onSuccess)){
		this.onSuccess(this.failedToStore);
	}
};

VTADrones.StoreMediaFromFeature.prototype.pop = function(){
	
	if(++this.index < this.featureMedia.length){
		return this.featureMedia[this.index];
	}
	
	return undefined;
};

VTADrones.StoreMediaFromFeature.prototype.startStoring = function(){
	
	this.storeNext();
};

VTADrones.StoreMediaFromFeature.prototype.storeNext = function(){
	
	var mediaFile = this.pop();
	
	if(mediaFile !== undefined){
		
		this.store(mediaFile);
	}else{
		
		this.storeComplete();
	}
};

VTADrones.StoreMediaFromFeature.prototype.addFailedToStore = function(failed){
	
	if(VTADrones.Util.existsAndNotNull(failed)){
		
		if(VTADrones.Util.existsAndNotNull(this.failedToStore)){
			this.failedToStore = [];
		}
		
		this.failedToStore.push(this.feature[VTADrones.FeatureTableHelper.ID], failed);
	}
};

VTADrones.StoreMediaFromFeature.prototype.store = function(mediaFile){
	
	var dataType = VTADrones.FailedSyncHelper.DATA_TYPES.MEDIA;
	var syncType = VTADrones.FailedSyncHelper.SYNC_TYPES.DOWNLOAD;
	
	var key = mediaFile;
	
	var context = this;
	
	VTADrones.FailedSyncHelper.insert(key, dataType, syncType,
			this.schema.getLayerId(), function(){
		
		context.storeNext();
	}, function(e){
		
		console.log("couldn't insert into failed_sync - " + JSON.Stringify(e));
		context.addFailedToStore(mediaFile);
		
		context.storeNext();
	});
};

