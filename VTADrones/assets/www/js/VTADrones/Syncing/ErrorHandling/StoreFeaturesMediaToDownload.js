VTADrones.StoreFeaturesMediaToDownload = function(_schema, _features, _onSuccess, _onFailure){
	this.schema = _schema;
	this.features = _features;
	this.onSuccess = _onSuccess;
	this.onFailure = _onFailure;
	this.failedToStore = null;
	this.index = -1;
};

VTADrones.StoreFeaturesMediaToDownload.prototype.allStorageComplete = function(){
	
	if(VTADrones.Util.funcExists(this.onSuccess)){
		this.onSuccess(this.failedToStore);
	}
};

VTADrones.StoreFeaturesMediaToDownload.prototype.startStoring = function(){
	
	this.storeNext();
};

VTADrones.StoreFeaturesMediaToDownload.prototype.pop = function(){
	
	if(++this.index < this.features.length){
		return this.features[this.index];
	}
	
	return undefined;
};

VTADrones.StoreFeaturesMediaToDownload.prototype.storeNext = function(){
	
	var feature = this.pop();
	
	if(feature !== undefined){
		
		this.store(feature);
	}else{
		console.log("storeFeaturesMedia storeNext feature is undefined");
		this.allStorageComplete();
	}
};

VTADrones.StoreFeaturesMediaToDownload.prototype.addToFailedToStore = function(id, failed){
	
	if(VTADrones.Util.existsAndNotNull(failed) && VTADrones.Util.existsAndNotNull(id)){
		
		if(!VTADrones.Util.existsAndNotNull(this.failedToStore)){
			this.failedToStore = {};
		}
		
		this.failedToStore[id] = failed;
	}
};

VTADrones.StoreFeaturesMediaToDownload.prototype.store = function(feature){
	var context = this;
	
	var storeMedia = new VTADrones.StoreMediaFromFeature(feature, this.schema, function(id, failed){
		
		console.log("store media from feature success: " + id + ", " + failed);
		context.addToFailedToStore(id, failed);
		
		context.storeNext();
	});
	
	storeMedia.startStoring();
};