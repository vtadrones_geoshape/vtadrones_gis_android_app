VTADrones.MediaSync = function(projectDb, _dbLayers, _layerSchemas, _mediaDir, _mediaToSend){
	this.projectDb = projectDb;
	this.mediaToSend = _mediaToSend;
	this.mediaDir = _mediaDir;
	
	this.layers = _dbLayers;
	
	this.layerSchemas = _layerSchemas;
	this.totalLayers = this.layers.length;
	
	this.index = -1;
	this.finishedLayersDownloading = 0;
	this.finishedLayersUploading = 0;
	
	this.onSyncComplete = null;
	this.onSyncFailure = null;
};

// url
// layerId
// auth headers

VTADrones.MediaSync.prototype.pop = function(){
	
	if(++this.index < this.layers.length){
		return this.layers[this.index];
	}
	
	return undefined;
};

VTADrones.MediaSync.prototype.startSync = function(onSuccess, onFailure, downloadOnly){
	var context = this;
	
	this.onSyncComplete = onSuccess;
	this.onSyncFailure = onFailure;

	if(downloadOnly === true || downloadOnly === "true"){
		
		context.startDownloadForNext();
	}else{
		
		context.startUploadForNext();
	}
};

VTADrones.MediaSync.prototype.onUploadComplete = function(){
	
	this.index = -1;
	
	this.startDownloadForNext();
};

VTADrones.MediaSync.prototype.onDownloadComplete = function(){
	
	if(VTADrones.Util.funcExists(this.onSyncComplete)){
		this.onSyncComplete();
	}
};

VTADrones.MediaSync.prototype.startUploadForNext = function(){
	var context = this;
	
	var layer = this.pop();
	
	if(layer !== undefined && (this.mediaToSend !== null 
			&& this.mediaToSend !== undefined)){
		
		this.uploadMedia(layer);
	}else{
		this.onUploadComplete();
	}
};

VTADrones.MediaSync.prototype.startDownloadForNext = function(){
	var context = this;
	
	var layer = this.pop();
	
	var downloadPhotos = false;
	
	var doWork = function() {
		if(layer !== undefined && downloadPhotos){
			context.downloadMedia(layer);
		}else{
			context.onDownloadComplete();
		}
	}
	
	VTADrones.PreferencesHelper.get(VTADrones.ProjectDbHelper.getProjectDatabase(), VTADrones.DOWNLOAD_PHOTOS, context, function(_downloadPhotos){
		if (_downloadPhotos !== undefined && _downloadPhotos !== null) {
			downloadPhotos = _downloadPhotos == 'true';
		}
		doWork();
	}, function(e){
		doWork();
	});
};

VTADrones.MediaSync.prototype.uploadMedia = function(layer){
	var context = this;
	
	var layerId = layer[VTADrones.LayersHelper.layerId()];
	var serverId = layer[VTADrones.LayersHelper.serverId()];
	
	var mediaForLayer = this.mediaToSend[layerId];
	
	if(mediaForLayer === null 
			|| mediaForLayer === undefined 
			|| mediaForLayer.length === 0){
		
		++this.finishedLayersUploading;
		
		if(this.finishedLayersUploading === this.totalLayers){
			
			var featureType = this.layerSchemas[layerId].getFeatureType();
			var finishedMediaCount = 0;
			var totalMediaCount = 0;
			
			VTADrones.Cordova.updateMediaUploadingStatus(featureType,
					finishedMediaCount, totalMediaCount,
					this.finishedLayersUploading, this.totalLayers);
		}
		
		this.startUploadForNext();
		
		return;
	}
	
	var server = VTADrones.Util.Servers.getServer(serverId);
	
	var schema = this.layerSchemas[layerId];
	
	if(schema.isEditable() === false || schema.isReadOnly()){
	
		++this.finishedLayersUploading;
		
		this.startUploadForNext();
		
		return;
	}
	
	var proceed = function(){
		
		++context.finishedLayersUploading;
		
		context.startUploadForNext();
	};
	
	var mediaUploader = new VTADrones.MediaUploader(this.projectDb,
			schema, this.mediaToSend,
			server, context.mediaDir,
			this.finishedLayersUploading, this.totalLayers);
	
	mediaUploader.startUpload(function(){
		
		proceed();
	}, function(e){
		
		// If the error was a timeout, call the sync failure callback to cancel the rest of the uploads
		if(e === VTADrones.Error.Sync.TIMED_OUT){
			
			if(VTADrones.Util.existsAndNotNull(context.onSyncFailure)){
				
				context.onSyncFailure(e);
			}
		}else{
			
			// Otherwise proceed with uploading the rest.
			proceed();
		}
	});
};

VTADrones.MediaSync.prototype.downloadMedia = function(layer){
	var context = this;
	
	var layerId = layer[VTADrones.LayersHelper.layerId()];
	var serverId = layer[VTADrones.LayersHelper.serverId()];

	var schema = this.layerSchemas[layerId];
	
	var server = VTADrones.Util.Servers.getServer(serverId);
	
	var featureDb = VTADrones.FeatureDbHelper.getFeatureDatabase();
	
	if(schema.isEditable() === false){
		
		++this.finishedLayersDownloading;
		
		this.startDownloadForNext();
		
		return;
	}
	
	var mediaDownloader = new VTADrones.MediaDownloader(featureDb,
			schema, server, this.mediaDir,
			this.finishedLayersDownloading, this.totalLayers);
	
	var proceed = function(){
		
		++context.finishedLayersDownloading;
		
		context.startDownloadForNext();
	};
	
	mediaDownloader.startDownload(function(){
		
		proceed();
	}, function(e){
		
		if(e === VTADrones.Error.Sync.TIMED_OUT){
			
			if(VTADrones.Util.existsAndNotNull(context.onSyncFailure)){
				context.onSyncFailure(e);
			}
		}else{
			proceed();
		}
	});
};