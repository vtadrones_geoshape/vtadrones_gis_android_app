VTADrones.Loaders.LayersLoader = (function(){
	var layersToLoad; // Number of layers
	var featuresLoadedFor; // Features loaded for how many layers
	var errorLoadingFeatures; // An array of feature types that couldn't be loaded
	
	var reset = function(){
		layersToLoad = 0;
		featuresLoadedFor = 0;
		errorLoadingFeatures = [];
	};
	
	var triggerDoneLoadingEvent = function(){
		var map = VTADrones.Map.getMap();
		
		map.events.triggerEvent(VTADrones.Loaders.
				LayersLoader.DONE_LOADING_LAYERS);
	};
	
	var isDone = function(onSuccess){
		
		if(featuresLoadedFor === layersToLoad){
			onSuccess();
			triggerDoneLoadingEvent();
		}
		
		if(errorLoadingFeatures.length > 0){
			console.log("DISPLAY ERROR LOADING FEATURES", errorLoadingFeatures);
			VTADrones.Cordova.errorLoadingFeatures(errorLoadingFeatures);
		}
	};
	
	var clearMap = function() {
		VTADrones.Layers.removeAllLayers();
	};
	
	var setBaseLayer = function(olBaseLayer){
		var map = VTADrones.Map.getMap();
		if (map.layers.length) {
			VTADrones.Layers.setNewBaseLayer(olBaseLayer);
			
			map.setLayerIndex(olBaseLayer, 0);
			
			if(!VTADrones.Util.existsAndNotNull(olBaseLayer.metadata)){
				olBaseLayer.metadata = {};
			}
			
			olBaseLayer.metadata.isBaseLayer = true;
		}
	};
	
	var loadWFSLayer = function(key, schema, _onSuccess){
		var olLayer = VTADrones.Layers.WFSLayer.create(key, schema);
		
		olLayer.metadata["onSaveStart"] = function(event) {
			var added = 0;
			var modified = 0;
			var removed = 0;
			for (var i = 0; i < event.features.length; i++){
				var feature = event.features[i];
				if (feature.metadata !== undefined && feature.metadata !== null) {
					if (feature.metadata.modified_state === VTADrones.FeatureTableHelper.MODIFIED_STATES.MODIFIED){
						modified++;
					} else if (feature.metadata.modified_state === VTADrones.FeatureTableHelper.MODIFIED_STATES.DELETED) {
						removed++;
					} else if (feature.metadata.modified_state === VTADrones.FeatureTableHelper.MODIFIED_STATES.INSERTED) {
						added++;
					}
				}
			}
			var getFeatureString = function(count) {
				if(count === 1){
					return VTADrones.Localization.localize('feature');
				}
				return VTADrones.Localization.localize('features');
			}
			var commitMsg = '';
			if (added > 0){
				commitMsg += VTADrones.Localization.localize('added') + ' ' + added + ' ' +
					getFeatureString(added);
			}
			if (modified > 0){
				if (added > 0){
					commitMsg += ', ';
				}
				commitMsg += VTADrones.Localization.localize('modified') + ' ' + modified + ' ' +
					getFeatureString(modified);
			}
			if (removed > 0){
				if (added > 0 || modified > 0){
					commitMsg += ', ';
				}
				commitMsg += VTADrones.Localization.localize('removed') + ' ' + removed + ' ' +
					getFeatureString(removed);
			}
			commitMsg += ' ' + VTADrones.Localization.localize('viaVTADrones') + '.';
			console.log(commitMsg);
			event.object.layer.protocol.options.handle = commitMsg;
		};
		
		VTADrones.Layers.addLayer(olLayer);
		
		olLayer.setVisibility(schema.isVisible());
		
		var onSuccess = function(){
			featuresLoadedFor++;
			isDone(_onSuccess);
		};
		
		var onFailure = function(){
			errorLoadingFeatures.push(schema.getFeatureType());
			onSuccess();
		};
		
		VTADrones.Loaders.FeaturesLoader.loadFeatures(schema,
				olLayer, onSuccess, onFailure);
	};
	
	var loadTMSLayer = function(key, schema){
		var olLayer = VTADrones.Layers.TMSLayer.create(key, schema);
		
		VTADrones.Layers.addLayer(olLayer);
		
		olLayer.setVisibility(schema.isVisible());
		
		return olLayer;
	};
	
	var loadWMSLayer = function(key, schema){
		var olLayer = VTADrones.Layers.WMSLayer.create(key, schema);
		
		VTADrones.Layers.addLayer(olLayer);
		
		var appDb = VTADrones.ApplicationDbHelper.getDatabase();
		
        VTADrones.PreferencesHelper.get(appDb, VTADrones.NO_CON_CHECKS, this, function(value) {
            if (value === 'true') {
                olLayer.setVisibility(schema.isVisible());
            } else {
                olLayer.setVisibility(schema.isVisible() && VTADrones.isOnline());
            }
        });
		
		return olLayer;
	};
	
	var addAOIToMap = function(_aoi){
		
		var context = this;
		
		var map = VTADrones.Map.getMap();
		
		var aoi = _aoi.split(',');
		
		var bounds = new OpenLayers.Bounds(aoi);
		
		var feature = new OpenLayers.Feature.Vector(bounds.toGeometry(), {});
		
		var oldLayers = map.getLayersByName(VTADrones.AOI);
		
		var aoiStyleMap = new OpenLayers.StyleMap({
             'default': new OpenLayers.Style({
                         fill: false,
                         strokeColor: 'red',
                         strokeWidth: 5
                 }) 
		});
		 
		var layer = new OpenLayers.Layer.Vector(VTADrones.AOI, {
			styleMap: aoiStyleMap 
		});
		
		layer.addFeatures([feature]);
		
		if(oldLayers.length > 0){
			map.removeLayer(oldLayers[0]);
		}
		
		map.addLayer(layer);
		
		// Make sure the aoi is the last layer and the highest
		map.setLayerIndex(layer, (map.layers.length - 1));
		//layer.setZIndex(726);
	};
	
	var loadAOILayer = function(){
		
		var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
		
		VTADrones.PreferencesHelper.get(projectDb, VTADrones.AOI, VTADrones.Loaders.LayersLoader, function(aoi){
			
			if(aoi !== null && aoi !== undefined && aoi !== ""){
				addAOIToMap(aoi);
			}
		}, function(e){
			console.log("VTADrones.Loaders.LayersLoader - Error loading aoi layer", e);
		});
	};
	
	var loadLayers = function(baseLayer, dbLayers, onSuccess){
		
		reset();
		
		clearMap();
		
		var layerSchemas = VTADrones.getLayerSchemas();
		
		var layer = null;
		var olBaseLayer = null;
		
		var disableWMS = false;
		
		var doWork = function() {
			if(!VTADrones.Util.existsAndNotNull(baseLayer)
			|| (VTADrones.Util.existsAndNotNull(baseLayer)
			&& baseLayer[VTADrones.BaseLayer.SERVER_NAME] === "OpenStreetMap")){
				
				olBaseLayer = VTADrones.Layers.addDefaultLayer(true);
			}
			
			if(layerSchemas === undefined 
					|| layerSchemas === null 
					|| (VTADrones.getLayerSchemasLength() === 0)){
				
				setBaseLayer(olBaseLayer);
				
				loadAOILayer();
				
				if(VTADrones.Util.funcExists(onSuccess)){
					isDone(onSuccess);
				}
				
				return;
			}
			
			var schema;
			var key;
			var editableLayers = 0;
			var featureType = null;
			var serverType = null;
			var isBaseLayer = false;
			
			for(var i = 0; i < dbLayers.length; i++){
				key = dbLayers[i][VTADrones.LayersHelper.layerId()];
				
				schema = layerSchemas[key];
				
				featureType = "";
				
				if(VTADrones.Util.existsAndNotNull(schema.getPrefix()) && schema.getPrefix() !== "null"){
					featureType += schema.getPrefix() + ":";
				}
				
				featureType += schema.getFeatureType();
				
				if(VTADrones.Util.existsAndNotNull(baseLayer) && (featureType === baseLayer[VTADrones.BaseLayer.FEATURE_TYPE])){
					isBaseLayer = true;
				}
				
				serverType = schema.getServerType();
				
				if(serverType === "WMS"){
					if (!disableWMS || isBaseLayer) {
						layer = loadWMSLayer(key, schema, isBaseLayer);
					}
				}else if(serverType === "TMS"){
					layer = loadTMSLayer(key, schema, isBaseLayer);
				}else{
					console.log("Invalid server type: " + serverType);
				}
				
				if(isBaseLayer === true){
					olBaseLayer = layer;
					isBaseLayer = false;
					dbLayers.splice(i--, 1);
				}else{
					if(serverType === "WMS"){
						layersToLoad++;
					}
				}
			}
			
			for(var i = 0; i < dbLayers.length; i++){
				
				key = dbLayers[i][VTADrones.LayersHelper.layerId()];
				
				schema = layerSchemas[key];
				
				if(schema.isEditable()){
					
					editableLayers++;
					// Load the vector layer
					loadWFSLayer(key, schema, onSuccess);
				}else{
					layersToLoad--;
					
					isDone(onSuccess);
				}
			}
			
			loadAOILayer();
			
			if(VTADrones.Util.existsAndNotNull(olBaseLayer)){
				setBaseLayer(olBaseLayer);
			}
			
			if(editableLayers === 0 
					&& VTADrones.Util.funcExists(onSuccess)){
				
				onSuccess();
			}
		}
		
		var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
		
		VTADrones.PreferencesHelper.get(projectDb, VTADrones.DISABLE_WMS, VTADrones.Loaders.LayersLoader, function(_disableWMS){
			if (_disableWMS !== undefined && _disableWMS !== null) {
				disableWMS = _disableWMS == 'true';
			}
			doWork();
		}, function(e){
			doWork();
		});
	};
	
	/**
	 * redraw the wfsLayers
	 */
	var redrawWFSLayers = function(){
		var map = VTADrones.Map.getMap();
		
		var wfsLayers = map.getLayersByClass("OpenLayers.Layer.Vector");
		
		for(var i = 0; i < wfsLayers.length; i++){
			wfsLayers[i].redraw();
		}
	};
	
	var checkSupportedCRS = function(baseLayer, dbLayers){
		
		var layerId = null;
		var schema = null;
		var proj4def = null;
		var crs = null;
		var unsupportedLayer = null;
		
		var schemas = VTADrones.getLayerSchemas();
		
		var layerTitleKey = VTADrones.LayersHelper.layerTitle();
		var workspaceKey = VTADrones.LayersHelper.workspace();
		var srsKey = VTADrones.GeometryColumnsHelper.featureGeometrySRID();
		var serverIdKey = VTADrones.LayersHelper.serverId();
		
		var unsupportedLayers = [];
		
		var obj = null;
		
		for(var i = 0; i < dbLayers.length; i++){
			
			layerId = dbLayers[i][VTADrones.LayersHelper.layerId()];
			
			schema = schemas[layerId];
			
			if(dbLayers[i][VTADrones.LayersHelper.featureType()] !== baseLayer[VTADrones.BaseLayer.FEATURE_TYPE] && schema.isEditable()){
				
				crs = schema.getSRID();
				
				proj4def = Proj4js.defs[crs];
				
				if(!VTADrones.Util.existsAndNotNull(proj4def)){
					
					// Add the layer to the list of unsupported layers
					// and decrement the index to continue iterating
					unsupportedLayer = dbLayers.splice(i--, 1);
					
					if(unsupportedLayer.constructor === Array){
						unsupportedLayer = unsupportedLayer[0];
					}
					
					obj = {};
					
					obj[layerTitleKey] = unsupportedLayer[layerTitleKey];
					obj[workspaceKey] = unsupportedLayer[workspaceKey];
					obj[serverIdKey] = unsupportedLayer[serverIdKey];
					obj[srsKey] = crs;
					
					unsupportedLayers.push(obj);
				}
			}else{
				console.log("its the baselayer or isn't editable!");
			}
		}
		
		console.log("unsupportedLayers", unsupportedLayers);
		
		return unsupportedLayers;
	};
	
	return {
		DONE_LOADING_LAYERS: "VTADrones_done_loading_layers",
		
		load: function(onSuccess, onFailure){
			var context = this;
			
			var layersWithUnsupportedCRS = null;
			
			// Load the servers
			VTADrones.ServersHelper.loadServers(context, function(){
				
				// Load the layers from the database
				VTADrones.LayersHelper.loadLayers(context, function(layers){
					
					var baseLayerLoader = new VTADrones.Loaders.BaseLayer();
					
					baseLayerLoader.load(function(baseLayer){
						
						// Load the layer schemas with layer data loaded from the db
						VTADrones.FeatureTableHelper.loadLayerSchemas(layers, function(){
							
							// Will return the unsupported layers and remove them from the layers array
							layersWithUnsupportedCRS = checkSupportedCRS(baseLayer, layers);
							
								// Load the layers onto the map
								loadLayers(baseLayer, layers, function(){
									
									var controlPanelHelper = new VTADrones.ControlPanelHelper();
									controlPanelHelper.getActiveControl(function(activeControl){
										
										controlPanelHelper.getLayerId(function(layerId){
											
											controlPanelHelper.getGeometryType(function(geometryType){
												
												if(activeControl == controlPanelHelper.CONTROLS.INSERT){
													VTADrones.Controls.ControlPanel.startInsertMode(layerId, geometryType);
												}
												
												if(VTADrones.Util.funcExists(onSuccess)){
													onSuccess();
												}
												
												// Sometimes after loading,
												// the wfs layers do not get drawn
												// properly.  This ensures they
												// get drawn correctly.
												redrawWFSLayers();
												
												if(VTADrones.Util.existsAndNotNull(layersWithUnsupportedCRS)
														&& layersWithUnsupportedCRS.length){
													
													VTADrones.Cordova.reportLayersWithUnsupportedCRS(layersWithUnsupportedCRS);
												}
											}, onFailure);
										}, onFailure)
									}, onFailure);
								});
						}, onFailure);
					}, onFailure);
				}, onFailure);
			}, onFailure);
		},
		
		addEventTypes: function(){
			var map = VTADrones.Map.getMap();
			
			map.events.addEventType(this.DONE_LOADING_LAYERS);
		}
	};
})();