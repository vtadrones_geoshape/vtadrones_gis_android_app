(function(){
	
	VTADrones.Loaders.BaseLayer = function(){
		
	};
	
	var prototype = VTADrones.Loaders.BaseLayer.prototype;
	
	prototype.load = function(onSuccess, onFailure){
		
		var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
		
		VTADrones.PreferencesHelper.get(projectDb, VTADrones.BASE_LAYER, this, function(baseLayer){
			
			if(VTADrones.Util.existsAndNotNull(baseLayer)){
				try{
					// base layer is stored as an array of json objects
					baseLayer = JSON.parse(baseLayer)[0];
				}catch(e){
					console.log(e.stack);
				}
			}else{
				var osm = "OpenStreetMap";
				
				baseLayer = {};
				
				baseLayer[VTADrones.BaseLayer.NAME] = "OpenStreetMap";
				baseLayer[VTADrones.BaseLayer.URL] = null;
				baseLayer[VTADrones.BaseLayer.SERVER_NAME] = "OpenStreetMap";
				baseLayer[VTADrones.BaseLayer.SERVER_ID] = "OpenStreetMap";
				baseLayer[VTADrones.BaseLayer.FEATURE_TYPE] = "";
			}
			
			if(VTADrones.Util.existsAndNotNull(onSuccess)){
				onSuccess(baseLayer);
			}
		}, function(e){
			if(VTADrones.Util.existsAndNotNull(onFailure)){
				onFailure(e);
			}
		});
	};
})();