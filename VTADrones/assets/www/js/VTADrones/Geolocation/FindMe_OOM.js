VTADrones.SavedFindMe = function(gotHighAccuracy, position){
	this.gotHighAccuracy = gotHighAccuracy;
	this.position = position;
};

VTADrones.FindMe_OOM = function(){};

VTADrones.FindMe_OOM.prototype.FINDME = "findme";

VTADrones.FindMe_OOM.prototype.savePoint = function(gotHighAccuracy, position, onSuccess, onFailure){
	
	var obj = new VTADrones.SavedFindMe(gotHighAccuracy, position);
	
	console.log("saving point: " + obj.gotHighAccuracy);
	
	var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
	
	VTADrones.PreferencesHelper.put(projectDb, this.FINDME,
			JSON.stringify(obj), this, function(){
		
		console.log("point saved successfully: " + obj.gotHighAccuracy);
		if(VTADrones.Util.funcExists(onSuccess)){
			onSuccess();
		}
	}, function(e){
		
		console.log("could not save point: " + obj.gotHighAccuracy);
		if(VTADrones.Util.funcExists(onFailure)){
			onFailure(e);
		}
	});
};

VTADrones.FindMe_OOM.prototype.getPoint = function(onSuccess, onFailure){
	var context = this;
	
	var success = function(findme){
		if(VTADrones.Util.funcExists(onSuccess)){
			
			if(VTADrones.Util.existsAndNotNull(findme)){
				onSuccess(JSON.parse(findme));
			}else{
				onSuccess(findme);
			}
		}
	};
	
	var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
	
	VTADrones.PreferencesHelper.get(projectDb, this.FINDME, this, function(findme){
		
		success(findme);
	}, function(e){
		
		if(VTADrones.Util.funcExists(onFailure)){
			onFailure(e);
		}
	});
};

VTADrones.FindMe_OOM.prototype.clearSavedPoint = function(onSuccess, onFailure){
	
	var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
	
	VTADrones.PreferencesHelper.remove(projectDb, this.FINDME, this, function(){
		
		if(VTADrones.Util.funcExists(onSuccess)){
			onSuccess();
		}
	}, function(e){
		
		if(VTADrones.Util.funcExists(onFailure)){
			onFailure(e);
		}
	});
};
