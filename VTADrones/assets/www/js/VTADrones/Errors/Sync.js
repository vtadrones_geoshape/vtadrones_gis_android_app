(function(){
	
	VTADrones.Error.Sync = {
		
		UNKNOWN_ERROR: 0,
		UPDATE_ERROR: 1,
		UNAUTHORIZED: 2,
		INTERNAL_SERVER_ERROR: 3,
		RESOURCE_NOT_FOUND: 4,
		TIMED_OUT: 5,
		VTADrones_ERROR: 6,
		MUST_COMPLETE_UPLOAD_FIRST: 7
	};
	
	VTADrones.Error.Sync.getErrorFromStatusCode = function(statusCode){
		var error = null;
		
		console.log("getErrorFromStatusCode: statusCode = " + statusCode);
		
		if(statusCode == 401){
			
			error = VTADrones.Error.Sync.UNAUTHORIZED;
		}else if(statusCode == 500){
			
			error = VTADrones.Error.Sync.INTERNAL_SERVER_ERROR;
		}else if(statusCode == 404){
			
			error = VTADrones.Error.Sync.RESOURCE_NOT_FOUND;
		}else{ // Bad gateway and gateway timeout... 
			
			error = VTADrones.Error.Sync.UNKNOWN_ERROR;
		}
		
		return error;
	};
})();