VTADrones.Cordova = (function() {
	
	var state = 0;
	
	var isNeutral = function(){
		return state === VTADrones.Cordova.STATES.NEUTRAL;
	};
	
	var waitForNeutralQueue = [];
	
	var waitingForReset = false;
	
	var waitForNeutral = function(func){
		if(!VTADrones.Util.funcExists(func)){
			return;
		}
		
		if(isNeutral()){
			func();
		}else{
			waitForNeutralQueue.push(func);
		}
	};
	
	var executeWaitingFuncs = function(){
		for(var func = waitForNeutralQueue.shift(); VTADrones.Util.funcExists(func);
			func = waitForNeutralQueue.shift()){
		
			func();
		}
	};
	
	return {

		STATES : {
				NEUTRAL: 0,
				CREATING_PROJECT: 1,
				UPDATING: 2,
				OUTSIDE_AOI_WARNING: 3,
				TAKING_PICTURE: 4
		},
		
		osmLinkClicked: function(){
			
			cordova.exec(null, null, "VTADronesCordova", "osmLinkClicked", []);
		},
		
		isAddingGeometryPart: function(isAddingPart){
			
			cordova.exec(null, null, "VTADronesCordova", "isAddingGeometryPart", [isAddingPart]);
		},
		
		gotPicture: function(){
			
			cordova.exec(null, null, "VTADronesCordova", "gotPicture", []);
		},
		
		syncOperationTimedOut: function(continueSync, cancelSync){
			
			cordova.exec(function(){
				
				if(VTADrones.Util.existsAndNotNull(continueSync)){
					continueSync();
				}
			}, function(){
				
				if(VTADrones.Util.existsAndNotNull(cancelSync)){
					cancelSync();
				}
			}, "VTADronesCordova", "syncOperationTimedOut", []);
		},
		
		featureNotInAOI: function(featureId, insertFeature, cancelInsertFeature){
			
			VTADrones.Cordova.setState(VTADrones.Cordova.STATES.OUTSIDE_AOI_WARNING);
			
			cordova.exec(function(){
				
				if(VTADrones.Util.existsAndNotNull(insertFeature)){
					
					insertFeature();
				}
				
				VTADrones.Cordova.setState(VTADrones.Cordova.STATES.NEUTRAL);
				
			}, function(){
				
				if(VTADrones.Util.existsAndNotNull(cancelInsertFeature)){
					cancelInsertFeature();
				}
				
				VTADrones.Cordova.setState(VTADrones.Cordova.STATES.NEUTRAL);
			}, "VTADronesCordova", "featureNotInAOI", [featureId]);
		},
		
		layersAlreadyInProject: function(layersAlreadyInProject){
			
			console.log("layersAlreadyInProject: " + JSON.stringify(layersAlreadyInProject));
			
			cordova.exec(null, null, "VTADronesCordova", "layersAlreadyInProject", [layersAlreadyInProject]);
		},
		
		appFinishedLoading: function(){
			
			cordova.exec(null, null, "VTADronesCordova", "appFinishedLoading", []);
		},
		
		gotNotifications: function(){
			
			cordova.exec(null, null, "VTADronesCordova", "gotNotifications", []);
		},
		
		finishedGettingLocation: function(){
			
			cordova.exec(null, null, "VTADronesCordova", "finishedGettingLocation", []);
		},
		
		/**
		 * Save the current maps extent
		 */
		resetWebApp : function(tx) {
			var bbox = VTADrones.Map.getCurrentExtent().toBBOX();
			
            var zoom = VTADrones.Map.getZoom();
			var reset = function(){
				console.log("resetWebApp: " + JSON.stringify(bbox));
				
				if(VTADrones.Util.existsAndNotNull(VTADrones.findme)){
					VTADrones.findme.clearIntervals();
					
					VTADrones.Cordova.finishedGettingLocation();
				}
				
				cordova.exec(null, null, "VTADronesCordova",
						"resetWebApp", [bbox, zoom]);
			};
			
			if(waitingForReset !== true){
				// Don't reset while a sync is occurring.
				waitForNeutral(function(){
					VTADrones.SQLiteTransactionManager.executeAfterDone(reset);
				});
				
				waitingForReset = true;
			}
		},
		
		getTileCount: function(){
			var bbox = VTADrones.Map.getCurrentExtent();
			
			var count = 0;
			
			cordova.exec(null, null, "VTADronesCordova",
					"confirmTileCount", [count]);
		},
		
		/**
		 * Get the area of interest in the aoi map and call the native method to
		 * save it to be set when onResume gets called on the MapActivity
		 */
		setProjectsAOI : function(layers) {
			var bbox = VTADrones.Map.getCurrentExtent();
			
			var count = 0;
			
			cordova.exec(null, null, "VTADronesCordova",
					"setProjectsAOI", [bbox.toBBOX(), count]);
		},
		
		setNewProjectsAOI: function(){
			var bbox = VTADrones.Map.getCurrentExtent().toBBOX();
			console.log("setNewProjectsAOI: bbox = " + bbox);
			cordova.exec(null, null, "VTADronesCordova",
					"setNewProjectsAOI", [bbox]);
		},
		
		goToProjects: function(){
			var bbox = VTADrones.Map.getCurrentExtent().toBBOX();
			
            var zoom = VTADrones.Map.getZoom();
			cordova.exec(null, null, "VTADronesCordova",
					"goToProjects", [bbox, zoom]);
		},
		
		createNewProject: function(){
			var bbox = VTADrones.Map.getCurrentExtent().toBBOX();
			
            var zoom = VTADrones.Map.getZoom();
			cordova.exec(null, null, "VTADronesCordova",
					"createNewProject", [bbox, zoom]);
		},
		
		errorCreatingProject: function(e){
			
			console.log("errorCreatingProject", e);
			cordova.exec(function(){
				VTADrones.Cordova.setState(VTADrones.Cordova.STATES.NEUTRAL);
			}, null, "VTADronesCordova",
					"errorCreatingProject", [e]);
		},
		
		errorLoadingFeatures: function(troublesomeFeatureTypes){
			var str = "";
			
			for(var i = 0; i < troublesomeFeatureTypes.length; i++){
				if(i > 0){
					str += troublesomeFeatureTypes[i];
				}else{
					str += ", " + troublesomeFeatureTypes[i];
				}
			}
			
			cordova.exec(null, null, "VTADronesCordova", "errorLoadingFeatures", [str]);
		},
		
		doneAddingLayers: function(){
			cordova.exec(null, null, "VTADronesCordova",
					"doneAddingLayers", []);
		},
		
		errorAddingLayers: function(e){
			cordova.exec(null, null, "VTADronesCordova",
					"errorAddingLayers", [e]);
		},
		
		// validate in here. 
		getUpdatedGeometry: function(){
			
			console.log("getUpdatedGeometry");
			
			VTADrones.Controls.ControlPanel.finishInserting(function(){
			
				console.log("finished inserting");
				
				// Also finishes modifying the geometry
				VTADrones.Controls.ControlPanel.exitModifyMode(function(){
					
					try{
						var selectedFeature = VTADrones.Controls
							.ControlPanel.getSelectedFeature();
					
						console.log("selectedFeature", selectedFeature);
						
						if(!VTADrones.Util.existsAndNotNull(selectedFeature)){
							
							cordova.exec(null, null, "VTADronesCordova", "invalidGeometriesEntered", [null, null]);
						}else{
							console.log("selectedFeature isn't null");
							var featureId = null;
							
							if(selectedFeature.metadata !== null 
									&& selectedFeature.metadata !== undefined){
								
								featureId = selectedFeature.metadata[
								    VTADrones.FeatureTableHelper.ID];
							}
							
							var layerId = VTADrones.Util.getLayerId(selectedFeature.layer);
							
							var schema = VTADrones.getLayerSchemas()[layerId];
							
							// Validate the feature and remove any invalid parts.
							var featureValidation = new VTADrones.Validation.Feature(selectedFeature, true);
							
							var invalidGeometries = featureValidation.validate();
							
							if(VTADrones.Util.existsAndNotNull(selectedFeature.metadata)
									&& selectedFeature.metadata[VTADrones.Validation.Feature.REMOVED_DURING_VALIDATION]){
								
								VTADrones.Controls.ControlPanel.setSelectedFeature(null);
								
								cordova.exec(null, null, "VTADronesCordova", "invalidGeometriesEntered", [schema.getFeatureType(), featureId]);
							}else{
								
								var insideAOI = featureValidation.checkFeatureAddedInsideAOI();
								
								var exec = function(){
									
									var wktGeometry = null;
									
									if(VTADrones.Util.existsAndNotNull(selectedFeature.geometry)){
										if(!VTADrones.Util.existsAndNotNull(featureId)){
											wktGeometry = VTADrones.Geometry.getNativeWKT(selectedFeature, layerId);
										}else{
											wktGeometry = VTADrones.Geometry.checkForGeometryCollection(layerId, featureId, schema.getSRID());
										}
									}
									
									cordova.exec(null, null, "VTADronesCordova", "showUpdatedGeometry",
											[schema.getFeatureType(), featureId, layerId, wktGeometry]);
								};
								
								if(insideAOI){
									exec();
								}else{
									
									VTADrones.Cordova.featureNotInAOI(featureId, exec, function(){
										
										// Using this to cancel the edit right now...
										VTADrones.Cordova.resetWebApp();
									});
								}
								
							}
						}
					}catch(e){
						console.log(e.stack);
					}
				});
			});
		},
		
		featureUnselected: function(){
			
			cordova.exec(null, null, "VTADronesCordova", "featureUnselected", []);
		},
		
		featureSelected : function(featureType, featureId, layerId,
				feature, mode, cancel){
			
			var featureValidator = new VTADrones.Validation.Feature(feature, false);
			
			featureValidator.validate();
			
			console.log("featureSelected featureValidator", featureValidator);
			
			if(featureValidator.hasValidGeometries === true){
				
				console.log("featureSelected: featureType = " + featureType 
						+ ", featureId = " + featureId + ", layerId = " 
						+ layerId + ", mode = " + mode + ", cancel = " 
						+ cancel + ", feature = ", feature);
				
				var schemas = VTADrones.getLayerSchemas();
				
				var schema = schemas[layerId];
				
				var wktGeometry = null;
				
				if(cancel === false){
					
					if(!VTADrones.Util.existsAndNotNull(featureId)){
						wktGeometry = VTADrones.Geometry.getNativeWKT(feature, layerId);
					}else{
						wktGeometry = VTADrones.Geometry.checkForGeometryCollection(layerId, featureId, schema.getSRID());
					}
				}
				
				// Put check in place because its causing issues when modifying new features.
				// Don't want it to stop editing...
				if(VTADrones.Util.existsAndNotNull(featureId)){
					VTADrones.Controls.ControlPanel.exitModifyMode();
				}
				
				console.log("displayFeatureDialog selectedFeature: ", VTADrones.Controls.ControlPanel.getSelectedFeature());
				
				cordova.exec(null, null, "VTADronesCordova", "featureSelected",
						[featureType, featureId, layerId, wktGeometry, mode]);
			}
		},
		
		updateTileSyncingStatus: function(percent){
			cordova.exec(null, null, "VTADronesCordova",
					"updateTileSyncingStatus", [percent]);
		},
		
		updateMediaUploadingStatus: function(featureType,
				finishedMediaCount, totalMediaCount,
				finishedLayerCount, totalLayerCount){
			
			console.log("updateMediaUploadingStatus featureType = " + featureType 
					+ ", finishedMediaCount = " + finishedMediaCount 
					+ ", totalMediaCount = " + totalMediaCount
					+ ", finishedLayerCount = " + finishedLayerCount 
					+ ", totalLayerCount = " + totalLayerCount);
			
			cordova.exec(null, null, "VTADronesCordova", "updateMediaUploadingStatus",
					[featureType, finishedMediaCount, totalMediaCount,
					 finishedLayerCount, totalLayerCount]);
		},
		
		updateMediaDownloadingStatus: function(featureType,
				finishedMediaCount, totalMediaCount,
				finishedLayerCount, totalLayerCount){
			
			console.log("updateMediaDownloadingStatus featureType = " + featureType 
					+ ", finishedMediaCount = " + finishedMediaCount 
					+ ", totalMediaCount = " + totalMediaCount
					+ ", finishedLayerCount = " + finishedLayerCount 
					+ ", totalLayerCount = " + totalLayerCount);
			
			cordova.exec(null, null, "VTADronesCordova","updateMediaDownloadingStatus",
					[featureType, finishedMediaCount, totalMediaCount,
					 finishedLayerCount, totalLayerCount]);
		},
		
		createProjectTileSyncingStatus: function(percent){
			cordova.exec(null, null, "VTADronesCordova",
					"createProjectTileSyncingStatus", [percent]);
		},
		
		syncCompleted: function(){
			console.log("syncCompleted");
			cordova.exec(function(){
				
				VTADrones.Cordova.resetWebApp();
				
				VTADrones.Cordova.setState(VTADrones.Cordova.STATES.NEUTRAL);
			}, null, "VTADronesCordova", "syncCompleted", []);
		},
		
		syncFailed: function(e){
			
			console.log("syncFailed");
			
			VTADrones.Cordova.setState(VTADrones.Cordova.STATES.NEUTRAL);
		},
		
		errorUpdatingAOI: function(e){
			
			cordova.exec(function(){
				VTADrones.Cordova.setState(VTADrones.Cordova.STATES.NEUTRAL);
			}, null, "VTADronesCordova", "errorUpdatingAOI", [e]);
		},
		
		getState: function(){
			return state;
		},
		
		setState: function(_state){
			state = _state;
			
			if(isNeutral()){
				executeWaitingFuncs();
			}
		},
		
		addMediaToFeature: function(key, media, fileName){
			
			cordova.exec(null , null, "VTADronesCordova",
					"addMediaToFeature", [key, media, fileName]);
			
			VTADrones.Cordova.setState(VTADrones.Cordova.STATES.NEUTRAL);
		},
		
		updateUploadingVectorDataProgress: function(finished, total){
			
			cordova.exec(null, null, "VTADronesCordova",
					"updateUploadingVectorDataProgress", [finished, total]);
		},
		
		updateDownloadingVectorDataProgress: function(finished, total){
			
			cordova.exec(null, null, "VTADronesCordova",
					"updateDownloadingVectorDataProgress", [finished, total]);
		},
		
		showDownloadingSchemasProgress: function(count){
			cordova.exec(null, null, "VTADronesCordova",
					"showDownloadingSchemasProgress", [count]);
		},
		
		updateDownloadingSchemasProgress: function(finished, total){
			
			cordova.exec(null, null, "VTADronesCordova",
					"updateDownloadingSchemasProgress", [finished, total]);
		},
		
		dismissDownloadingSchemasProgress: function(){
			
			cordova.exec(null, null, "VTADronesCordova",
					"dismissDownloadingSchemasProgress", []);
		},
		
		alertGeolocationError: function(){
			
			cordova.exec(null, null, "VTADronesCordova",
					"alertGeolocationError", []);
		},
		
		setMultiPartBtnsEnabled: function(enable, enableCollection){
			
			cordova.exec(null, null, "VTADronesCordova", "setMultiPartBtnsEnabled", [enable, enableCollection]);
		},
		
		confirmPartRemoval: function(onConfirm){
			
			cordova.exec(onConfirm, null, "VTADronesCordova", "confirmPartRemoval", []);
		},
		
		confirmGeometryRemoval: function(onConfirm){
			
			cordova.exec(onConfirm, null, "VTADronesCordova", "confirmGeometryRemoval", []);
		},
		
		hidePartButtons: function(){
			cordova.exec(null, null, "VTADronesCordova", "hidePartButtons", []);
		},
		
		reportLayersWithUnsupportedCRS: function(layers){
			cordova.exec(null, null, "VTADronesCordova", "reportLayersWithUnsupportedCRS", [layers]);
		}
	};
})();