VTADrones.SQLiteTransactionManager = (function(){
	var dbs = [];
	
	var onAllCompleted = [];
	
	var addToOnCompleted = function(func){
		onAllCompleted.push(func);
	};
	
	var isDone = function(){
		for(var i = 0; i < dbs.length; i++){
			if(!dbs[i].isDone()){
				return false;
			}
		}
		
		return true;
	};
	
	var executeOnAllCompleted = function(){
		var func = null;
		
		for(func = onAllCompleted.shift(); VTADrones.Util.funcExists(func);
			func = onAllCompleted.shift()){
			
			func();
		}
	};
	
	// Make sure that all transactions are completed
	var transactionCompleted = function(){
		if(isDone()){
			executeOnAllCompleted();
		}
	};
	
	return {
		push: function(_db){
			dbs.push(new VTADrones.SQLiteTransaction(_db,
					transactionCompleted));
		},
		
		executeAfterDone: function(func){
			if(!VTADrones.Util.funcExists(func)){
				return;
			}
			
			if(isDone()){
				func();
			}else{
				addToOnCompleted(func);
			}
		}
	};
})();