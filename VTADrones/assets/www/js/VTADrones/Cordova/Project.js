VTADrones.Cordova.Project = (function(){
	var describeFeatureTypeReader = new OpenLayers.Format.WFSDescribeFeatureType();
	
	var includeOOMWorkaround = true;
	
	var syncInProgress = false;
	
	var gettingUsersLocation = false;
	
	var getSchemaHelper = function(specificSchemas, layerId){
		
		specificSchemas.push(VTADrones.getLayerSchemas()[layerId]);
	};
	
	// This is awful, but for the sake of time...
	var layersAlreadyInProject = null;
	
	var getSchemasFromDbLayers = function(dbLayers){
		var specificSchemas = [];
		
		var layerId = null;
		var VTADronesSchemas = VTADrones.getLayerSchemas();
		
		for(var i = 0; i < dbLayers.length; i++){
			layerId = dbLayers[i][VTADrones.LayersHelper.layerId()];
			
			getSchemaHelper(specificSchemas, layerId);
		}
		
		return specificSchemas;
	};
	
	var prepareSync = function(layers, bounds, cacheTiles, onSuccess, onFailure){
		
		VTADrones.Loaders.LayersLoader.load(function(){
			
			if(bounds !== "" && bounds !== null && bounds !== undefined){
				
				var specificSchemas = getSchemasFromDbLayers(layers);
				
				for(var i = 0; i < specificSchemas.length; i++){
					console.log("schema featureType = " + specificSchemas[i].getFeatureType());
				}
				
				VTADrones.Cordova.Project.sync(cacheTiles, true,
						specificSchemas, onSuccess, onFailure);
			}else{
				if(VTADrones.Util.funcExists(onSuccess)){
					onSuccess();
				}
			}
		}, function(e){
			if(VTADrones.Util.funcExists(onFailure)){
				onFailure(e);
			}
		});
	};
	
	var storeFeatureData = function(layers, bounds, cacheTiles, onSuccess, onFailure){
		
		var schemaDownloader = new VTADrones.SchemaDownloader(layers, VTADrones.WFS_DFT_VERSION, function(_layersAlreadyInProject){
			
			layersAlreadyInProject = _layersAlreadyInProject;
			
			prepareSync(layers, bounds, cacheTiles, onSuccess, onFailure);
		}, function(e){
			if(VTADrones.Util.funcExists(onFailure)){
				onFailure(e);
			}
		});
		
		schemaDownloader.startDownload();
	};
	
	var storeData = function(context, layers, bounds, cacheTiles, onSuccess, onFailure){
		VTADrones.ServersHelper.loadServers(context, function(){
			storeFeatureData(layers, bounds, cacheTiles, onSuccess, onFailure);
		}, onFailure);
	};
	
	var zoomToExtent = function(savedBounds, savedZoom){
		var bounds = savedBounds.split(',');
		
		VTADrones.Map.zoomToExtent(bounds[0], bounds[1],
				bounds[2], bounds[3], savedZoom);
	};
	
	return {
		createProject: function(layers){
			var context = this;
			
			VTADrones.Cordova.setState(VTADrones.Cordova.STATES.CREATING_PROJECT);
			
			var onSuccess = function(){
				
				VTADrones.Cordova.syncCompleted();
			};
			
			var onFailure = function(e){
				console.log("VTADrones.Cordova.Project", e);
			//	VTADrones.Cordova.errorCreatingProject(e);
				VTADrones.Cordova.syncCompleted();
			};
			
			VTADrones.ProjectDbHelper.getProjectDatabase().close();
			VTADrones.FeatureDbHelper.getFeatureDatabase().close();
			
			var db = VTADrones.ProjectDbHelper.getProjectDatabase();
			
			VTADrones.Cordova.Project.updateBaseLayer(function(){
				VTADrones.PreferencesHelper.get(db, VTADrones.AOI, this, function(_aoi){
					var bounds = null;
					
					if(_aoi !== null && _aoi !== undefined 
							&& _aoi !== ""){
						
						var aoi = _aoi.split(',');
						
						bounds = new VTADrones.Util.Bounds(aoi[0],
							aoi[1], aoi[2], aoi[3]);
					}
					
					storeData(context, layers, bounds, true, function(){
						onSuccess();
					}, onFailure);
				}, onFailure);
			}, onFailure);
		},
		
		cacheBaseLayer: function(){
			
			var context = this;
			
			if(syncInProgress){
				
				console.log("sync already in progress");
				
				return;
			}
			
			var fail = function(e){
				
				console.log("sync failed", e);
				
				if(syncInProgress){
					VTADrones.Cordova.syncCompleted();
				}
				
				syncInProgress = false;
			};
			
			VTADrones.Cordova.setState(VTADrones.Cordova.STATES.UPDATING);
			
			VTADrones.Cordova.Project.updateBaseLayer(function(){

				var baseLayerLoader = new VTADrones.Loaders.BaseLayer();
				
				baseLayerLoader.load(function(baseLayer){
					
					VTADrones.Loaders.LayersLoader.load(function(){
						
						var db = VTADrones.ProjectDbHelper.getProjectDatabase();
						
						VTADrones.PreferencesHelper.get(db, VTADrones.AOI, context, function(_aoi){
							
							if(_aoi !== null && _aoi !== undefined 
									&& _aoi !== ""){
								
								var aoi = _aoi.split(',');
								
								var bounds = new VTADrones.Util.Bounds(aoi[0], aoi[1], aoi[2], aoi[3]);
									
								var map = VTADrones.Map.getMap();
								
								var syncHelper = new VTADrones.Sync(map, bounds, true, function(){
									
									syncInProgress = false;
									
									VTADrones.Cordova.syncCompleted();
								}, fail, VTADrones.FileSystem.getFileSystem(), baseLayer, true,
								VTADrones.ProjectDbHelper.getProjectDatabase(), VTADrones.FeatureDbHelper.getFeatureDatabase());
								
								syncInProgress = true;
								
								syncHelper.startTileCache();
							}
						}, fail);
					}, fail);
				}, fail);
			}, fail);
		},
		
		updateBaseLayer: function(onSuccess, onFailure){
			var baseLayerLoader = new VTADrones.Loaders.BaseLayer();
			
			var fail = function(e){
				console.log("Error changing base layer: " + e);
				if(VTADrones.Util.existsAndNotNull(onFailure)){
					onFailure(e);
				}
			};
			
			baseLayerLoader.load(onSuccess, fail);
		},
		
		addLayers: function(layers){
			var context = this;
			
			VTADrones.Cordova.setState(VTADrones.Cordova.STATES.UPDATING);
			
			var onSuccess = function(){
				
				if(VTADrones.Util.existsAndNotNull(layersAlreadyInProject) && layersAlreadyInProject.length > 0){
					VTADrones.Cordova.layersAlreadyInProject(layersAlreadyInProject);
				}
				
				layersAlreadyInProject = null;
				
				VTADrones.Cordova.syncCompleted();
			};
			
			var onFailure = function(e){
			//	VTADrones.Cordova.errorAddingLayers(e);
				VTADrones.Cordova.syncCompleted();
				
				layersAlreadyInProject = null;
			};
			
			var db = VTADrones.ProjectDbHelper.getProjectDatabase();
			
			VTADrones.PreferencesHelper.get(db, VTADrones.AOI, context, function(_aoi){
				var aoi = _aoi.split(','); 
				var bounds = null;
				
				if(_aoi !== null && _aoi !== undefined && _aoi !== ""){
					bounds = new VTADrones.Util.Bounds(aoi[0], aoi[1], aoi[2], aoi[3]);
				}
				
				storeData(context, layers, bounds, false, function(){
					
					onSuccess();
				}, onFailure);
			}, onFailure);
		},
		
		updateAOI: function(left, bottom, right, top){
			var aoi = left + ", " + bottom 
				+ ", " + right + ", " + top;
			
			// onSyncFailure execute the native
			// method to report the error to the
			// user.
			var onFailure = function(e){
				VTADrones.Cordova.errorUpdatingAOI(e);
			};
			
			var db = VTADrones.ProjectDbHelper.getProjectDatabase();
			
			VTADrones.PreferencesHelper.put(db, VTADrones.AOI, aoi, this, function(){
				
				VTADrones.Cordova.Project.sync(true);
			}, onFailure);
		},
		
		getSavedBounds: function(onSuccess, onFailure){
			
			var db = VTADrones.ProjectDbHelper.getProjectDatabase();
			
			VTADrones.PreferencesHelper.get(db, VTADrones.SAVED_BOUNDS, this, function(savedBounds){
				VTADrones.PreferencesHelper.get(db, VTADrones.SAVED_ZOOM_LEVEL, this, function(savedZoom){
					
					if(VTADrones.Util.funcExists(onSuccess)){
						onSuccess(savedBounds, savedZoom);
					}
				}, onFailure);
			}, onFailure);
		},
		
		zoomToAOI: function(onSuccess, onFailure){
			
			var db = VTADrones.ProjectDbHelper.getProjectDatabase();
			
			VTADrones.PreferencesHelper.get(db, VTADrones.AOI, this, function(_aoi){
				
				if(_aoi !== null && _aoi !== undefined 
						&& _aoi !== ""){
					
					var aoi = _aoi.split(',');
					
					VTADrones.Map.zoomToExtent(aoi[0],
							aoi[1], aoi[2], aoi[3]);
				}else{
					VTADrones.Cordova.Project.zoomToDefault();
				}
				
				
				if(VTADrones.Util.funcExists(onSuccess)){
					onSuccess();
				}
			}, onFailure);
		},
		
		zoomToDefault: function(){
			zoomToExtent(VTADrones.DEFAULT_ZOOM_EXTENT);
		},
		
		zoomToCurrentPosition: function(onSuccess, onFailure){
			
			if(!gettingUsersLocation){
			
				try{
					var map = VTADrones.Map.getMap();
					
					var aoiLayer = map.getLayersByName(VTADrones.AOI)[0];
					
					if(!VTADrones.Util.funcExists(aoiLayer)){
						throw "AOI layer does not exist";
					}
					
					gettingUsersLocation = true;
					
					var onDone = function(){
						
						gettingUsersLocation = false;
						
						VTADrones.Cordova.finishedGettingLocation();
					};
					
					if(!VTADrones.Util.existsAndNotNull(VTADrones.findme)){
						VTADrones.findme = new VTADrones.FindMe(map, aoiLayer);
					}
					
					VTADrones.findme.getLocation(onDone, function(e){
						
						onDone();
						
						VTADrones.Cordova.alertGeolocationError();
					});
					
				}catch(e){
					console.log(e);
				}
			}
		},
		
		sync: function(_cacheTiles, _downloadOnly, _specificSchemas, onSuccess, onFailure){
			console.log("sync");
			
			var context = this;
			
			if(syncInProgress === true){
				
				console.log("sync is already in progress!");
				
				return;
			}
			
			var map = VTADrones.Map.getMap();
			var cacheTiles = _cacheTiles;
			var downloadOnly = _downloadOnly;
			var specificSchemas = _specificSchemas;
			
			if(cacheTiles === null || cacheTiles === undefined){
				cacheTiles = false;
			}
			
			if(downloadOnly === null || downloadOnly === undefined){
				downloadOnly = false;
			}
			
			if(VTADrones.getLayerSchemasLength() > 0 ||
					((downloadOnly === true || downloadOnly === "true")
							&& specificSchemas.length > 0) || cacheTiles){
				
				VTADrones.Cordova.setState(VTADrones.Cordova.STATES.UPDATING);
				
				var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
				
				VTADrones.PreferencesHelper.get(projectDb, VTADrones.AOI, context, function(_aoi){
					
					if(_aoi !== null && _aoi !== undefined 
							&& _aoi !== ""){
						
						var aoi = _aoi.split(',');
						
						var bounds = new VTADrones.Util.Bounds(aoi[0], aoi[1], aoi[2], aoi[3]);
						
						VTADrones.PreferencesHelper.get(projectDb, VTADrones.BASE_LAYER, context, function(baseLayer){
							
							if(VTADrones.Util.existsAndNotNull(baseLayer)){
								try{
									// base layer is stored as an array of json objects
									baseLayer = JSON.parse(baseLayer)[0];
								}catch(e){
									console.log(e.stack);
								}
							}
							
							var syncHelper = new VTADrones.Sync(map, bounds, downloadOnly, function(){
								
								syncInProgress = false;
								
								if(VTADrones.Util.funcExists(onSuccess)){
									onSuccess();
								}else{
									VTADrones.Cordova.syncCompleted();
								}
							}, function(e){
								
								console.log("sync failed", e);
								
								syncInProgress = false;
								
								if(VTADrones.Util.funcExists(onFailure)){
									onFailure(e);
								}else{
									VTADrones.Cordova.syncCompleted();
								}
							}, VTADrones.FileSystem.getFileSystem(), baseLayer, cacheTiles,
							VTADrones.ProjectDbHelper.getProjectDatabase(), VTADrones.FeatureDbHelper.getFeatureDatabase());
							
							if(downloadOnly === true || downloadOnly === "true"){
								
								syncHelper.setSpecificSchemas(specificSchemas);
							}
							
							syncInProgress = true;
							
							syncHelper.sync();
						}, function(e){
							if(VTADrones.Util.funcExists(onFailure)){
								onFailure(e);
							}
						});
					}
				}, function(e){
					
					if(VTADrones.Util.funcExists(onFailure)){
						onFailure(e);
					}
				});
			}else{
				if(VTADrones.Util.funcExists(onSuccess)){
					onSuccess();
				}else{
					VTADrones.Cordova.syncCompleted();
				}
			}
		},
		
		getNotifications: function(syncId){
			
			console.log("getNoficiations: syncId = " + syncId);
			
			var context = this;
			
			if(syncInProgress){
				
				console.log("sync already in progress");
				
				return;
			}
			
			var fail = function(e){
				
				console.log("sync failed", e);
				
				if(syncInProgress){
					VTADrones.Cordova.syncCompleted();
				}
				
				syncInProgress = false;
			};
			
			var projectDb = VTADrones.ProjectDbHelper.getProjectDatabase();
			
			VTADrones.PreferencesHelper.get(projectDb, VTADrones.AOI, context, function(_aoi){
				
				if(_aoi !== null && _aoi !== undefined 
						&& _aoi !== ""){
					
					var aoi = _aoi.split(',');
					
					var bounds = new VTADrones.Util.Bounds(aoi[0], aoi[1], aoi[2], aoi[3]);
						
					var map = VTADrones.Map.getMap();
					
					var syncHelper = new VTADrones.Sync(map, bounds, false, function(){
						
						syncInProgress = false;
						
						VTADrones.Cordova.gotNotifications();
					}, fail, VTADrones.FileSystem.getFileSystem(), null, false,
					VTADrones.ProjectDbHelper.getProjectDatabase(), VTADrones.FeatureDbHelper.getFeatureDatabase());
					
					syncInProgress = true;
					
					VTADrones.Cordova.setState(VTADrones.Cordova.STATES.UPDATING);
					
					syncHelper.syncId = syncId;
					
					syncHelper.getNotifications();
				}
			}, fail);
		}
	};
})();