VTADrones.FileSystem = (function(){
	var fileSystem = null;
	
	var createMediaDirectories = function(projectName, onSuccess, onFailure){
		var path = VTADrones.FileSystem.ROOT_LEVEL;
		
		fileSystem.root.getDirectory(VTADrones.FileSystem.ROOT_LEVEL, {create: true}, function(dir){
			
			path += VTADrones.FileSystem.fileSeparator + VTADrones.FileSystem.PROJECTS;
			
			fileSystem.root.getDirectory(path, {create: true}, function(dir){
				
				path += VTADrones.FileSystem.fileSeparator + projectName;
				
				fileSystem.root.getDirectory(path,
						{create: true}, function(dir){
					
					path += VTADrones.FileSystem.fileSeparator + VTADrones.FileSystem.MEDIA;
					
					fileSystem.root.getDirectory(path, {create: true}, function(mediaDir){
						
						console.log("createMediaDirectory toURL() = " + mediaDir.toURL());
						
						if(VTADrones.Util.funcExists(onSuccess)){
							onSuccess(mediaDir);
						}
						
					}, function(){
						if(VTADrones.Util.funcExists(onFailure)){
							onFailure("Error getting " + path + " directory");
						}
					});	
					
				}, function(){
					if(VTADrones.Util.funcExists(onFailure)){
						onFailure("Error getting " + path + " directory");
					}
				});		
				
			}, function(){
				if(VTADrones.Util.funcExists(onFailure)){
					onFailure("Error getting " + path + " directory");
				}
			});
		}, function(){
			if(VTADrones.Util.funcExists(onFailure)){
				onFailure("Error getting " + path + " directory");
			}
		});
	};
	
	return{
		NATIVE_ROOT_URL: null,
		
		ROOT_LEVEL : "VTADrones",
		
		TILESET_ROOT : "TileSets",
		
		// Media dirs begin
		PROJECTS : "Projects",
		MEDIA : "Media",
		fileSeparator : "/",
		
		// Media dirs end.
		
		setFileSystem: function(onSuccess, onFailure){
			var fail = function(e){
				
				if(VTADrones.Util.funcExists(onFailure)){
					onFailure(e.code);
				}
			};
			
			window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(_fileSystem){
				fileSystem = _fileSystem;
				
				window.resolveLocalFileSystemURL("cdvfile://localhost/persistent/", function(fileEntry){
					
					VTADrones.FileSystem.NATIVE_ROOT_URL = fileEntry.nativeURL;
					
					if(VTADrones.Util.funcExists(onSuccess)){
						onSuccess(fileSystem);
					}
				}, fail);
			}, fail);
		},
		
		getFileSystem: function(){
			return fileSystem;
		},
		
		ensureMediaDirectoryExists: function(onSuccess, onFailure){
			
			var db = VTADrones.ProjectDbHelper.getProjectDatabase();
			
			VTADrones.PreferencesHelper.get(db, VTADrones.PROJECT_NAME, VTADrones.FileSystem, function(projectName){
				
				// Make sure the directories being 
				// used with the file api exist.
				createMediaDirectories(projectName, function(mediaDir){
					if(VTADrones.Util.funcExists(onSuccess)){
						onSuccess(mediaDir);
					}
				}, onFailure);
				
			}, function(e){
				if(VTADrones.Util.funcExists(onFailure)){
					onFailure("VTADrones.FileSystem createMediaDirectories ERROR - " + e);
				}
			});
		}
	};
})();