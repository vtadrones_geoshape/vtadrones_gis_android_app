(function(){
	
	VTADrones.TileDir = function(fileSystem, baseLayer){
		this.baseLayer = baseLayer;
		this.fileSystem = fileSystem;
	};
	
	var prototype = VTADrones.TileDir.prototype;
	
	prototype.getTileSetRoot = function(){
		return VTADrones.FileSystem.ROOT_LEVEL
			+ VTADrones.FileSystem.fileSeparator
			+ VTADrones.FileSystem.TILESET_ROOT;
	};
})();