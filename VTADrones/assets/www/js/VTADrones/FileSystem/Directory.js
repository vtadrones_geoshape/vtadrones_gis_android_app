(function(){
	
	VTADrones.Directory = function(fileSystem, path){
		this.fileSystem = fileSystem;
		this.path = path;
		this.dir = null;
	};
	
	var prototype = VTADrones.Directory.prototype;
	
	prototype.getDirectory = function(options, onExists, onNotExists, parentDir, path){
		
		if(VTADrones.Util.existsAndNotNull(this.dir)){
			return this.dir;
		}
		
		var context = this;
		
		if(!VTADrones.Util.existsAndNotNull(path)){
			path = this.path;
		}
		
		if(path.constructor === String){
			path = path.split('/');
		}
		
		if(!VTADrones.Util.existsAndNotNull(parentDir)){
			parentDir = this.fileSystem.root;
		}
		
		if(!VTADrones.Util.existsAndNotNull(options)){
			options = {
				create: false
			};
		}
		
		var part = path.shift();
		
		if(VTADrones.Util.existsAndNotNull(part)){
			
			parentDir.getDirectory(part, options, function(dir){
				
				if(path.length > 0){
					context.getDirectory(options, onExists, onNotExists, dir, path);
				}else{
					if(VTADrones.Util.existsAndNotNull(onExists)){
						context.dir = dir;
						onExists(dir);
					}
				}
			}, function(e){
				if(e.code === FileError.NOT_FOUND_ERR){
					if(VTADrones.Util.existsAndNotNull(onNotExists)){
						onNotExists();
					}
				}else{
					if(VTADrones.Util.existsAndNotNull(onNotExists)){
						console.log("Couldn't get directory", e);
						
						onNotExists(e);
					}
				}
			});
		}
	};
})();